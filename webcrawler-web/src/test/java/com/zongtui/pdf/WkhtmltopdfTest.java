package com.zongtui.pdf;

import java.util.logging.Logger;

import org.junit.Test;

import com.zongtui.web.common.cmd.RunCmd;

/**
 * ClassName: WkhtmltopdfTest <br/>
 * Function: 转换html到pdf. <br/>
 * date: 2015年5月1日 下午7:00:49 <br/>
 *
 * @author liuyuqi
 * @version
 * @since JDK 1.7
 */
public class WkhtmltopdfTest {
	String result = null;
	String format = "--outline";
	static Logger logger = Logger.getLogger(WkhtmltopdfTest.class.getName());

	/**
	 * 测试函数，一个简单的测试实例
	 */
	@Test
	public void saveTest1() {
		// String website = "http://my.oschina.net/yutian9793/admin/inbox";
		String website = "http://www.cnblogs.com/skyme/p/4366397.html";
		String savePath = "D:/文档分享流程.pdf";
		String wkhtmltopdfPath = "D:\\Program Files\\wkhtmltopdf";
		saveAsPDFDefault(website, savePath, wkhtmltopdfPath);
		System.out.println(result + "covent finish");
	}

	/**
	 * saveTest2:(测试saveAsPDFWithParameters()的方法). <br/>
	 *
	 * @author liuyuqi
	 * @since JDK 1.7
	 */
	@Test
	public void saveTest2() {
		String website = "http://m.blog.csdn.net/blog/hongweibing1/39251059";
		String savePath = "D:/aa.pdf";
		String wkhtmltopdfPath = "D:\\Program Files\\wkhtmltopdf";
		// String parameters[] = { "yes", "yes", "标题", "yes" };
		// String parameters[] = { "no", "yes", "标题", "no" };
		String parameters[] = { "no", "yes", "标题", "yes" };
		saveAsPDFWithParameters(website, savePath, wkhtmltopdfPath, parameters);
		System.out.println(result + "covent finish");
	}

	/**
	 * 网页保存为PDF，默认模式
	 * 
	 * @param website
	 *            网站地址
	 * @param savePath
	 *            保持路径，目录不支持中文！！！
	 * @param wkhtmltopdfPath
	 *            安装wkhtmltopdf的路径
	 * @return 返回命令行运行值
	 */
	public String saveAsPDFDefault(String website, String savePath,
			String wkhtmltopdfPath) {

		result = "transform begin";
		String cmd = "wkhtmltopdf " + website + " " + savePath;
		result = RunCmd.pathCmd(wkhtmltopdfPath, cmd);
		return result;
	}

	/**
	 * saveAsPDFWithParameters:(带参数配置保存为pdf的方法，使用方法参看saveTest2()). <br/>
	 *
	 * @author liuyuqi
	 * @param website
	 *            网站地址
	 * @param savePath
	 *            保持路径，目录不支持中文！！！
	 * @param wkhtmltopdfPath
	 *            安装wkhtmltopdf的路径
	 * @param parameters
	 *            系列参数：（1）是否生成目录[yes/on]（2）是否去掉链接[yes/no]（3）设置页眉[title]（4）
	 *            是否显示页脚[yes/no]
	 * @return 返回命令行运行值
	 * @since JDK 1.7
	 */
	public String saveAsPDFWithParameters(String website, String savePath,
			String wkhtmltopdfPath, String[] parameters) {
		result = "transform begin";
		String cmd = "wkhtmltopdf ";//
		StringBuilder stringBuilder = new StringBuilder(cmd);
		switch (parameters[0]) {
		case "yes":
			stringBuilder.append(" --outline ");
			break;
		case "no":
			break;
		default:
			System.out.println("第一个参数错误！,该参数将不生效！pdf是否添加目录，请填yes或者no");
		}
		switch (parameters[1]) {
		case "yes":
			stringBuilder.append(" --disable-external-links ");
			break;
		case "no":
			break;
		default:
			System.out.println("第二个参数错误！,该参数将不生效！pdf是否去掉所有链接，请填yes或者no");
		}
		if (parameters[2].length() > 0) {
			stringBuilder.append(" --header-center " + '"' + parameters[2]
					+ '"' + " --header-line ");
		} else {
			System.out.println("第三个参数错误！,该参数将不生效！pdf是否添加页眉，添加的话请输入标题");
		}
		switch (parameters[3]) {
		case "yes":
			stringBuilder.append(" --footer-center '[page]/[topage]' ");
			break;
		case "no":
			break;
		default:
			System.out.println("第四个参数错误！,该参数将不生效！pdf是否添加页脚，请填yes或者no");
		}
		stringBuilder.append('"' + website + '"' + " " + savePath);
		cmd = stringBuilder.toString();
		System.out.println(cmd);
		result = RunCmd.pathCmd(wkhtmltopdfPath, cmd);
		return result;
	}
}
