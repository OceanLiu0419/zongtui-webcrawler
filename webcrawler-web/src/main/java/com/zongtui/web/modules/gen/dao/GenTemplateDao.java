/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.zongtui.web.modules.gen.dao;

import com.zongtui.web.common.persistence.CrudDao;
import com.zongtui.web.common.persistence.annotation.MyBatisDao;
import com.zongtui.web.modules.gen.entity.GenTemplate;

/**
 * 代码模板DAO接口
 * @author ThinkGem
 * @version 2013-10-15
 */
@MyBatisDao
public interface GenTemplateDao extends CrudDao<GenTemplate> {
	
}
