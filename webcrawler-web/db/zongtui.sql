/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : zongtui

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2015-05-03 21:51:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `echartsdemo`
-- ----------------------------
DROP TABLE IF EXISTS `echartsdemo`;
CREATE TABLE `echartsdemo` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of echartsdemo
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_scheme`
-- ----------------------------
DROP TABLE IF EXISTS `gen_scheme`;
CREATE TABLE `gen_scheme` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `category` varchar(2000) DEFAULT NULL COMMENT '分类',
  `package_name` varchar(500) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(500) DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(100) DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(100) DEFAULT NULL COMMENT '生成功能作者',
  `gen_table_id` varchar(200) DEFAULT NULL COMMENT '生成表编号',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`),
  KEY `gen_scheme_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='生成方案';

-- ----------------------------
-- Records of gen_scheme
-- ----------------------------
INSERT INTO `gen_scheme` VALUES ('35a13dc260284a728a270db3f382664b', '树结构', 'treeTable', 'com.thinkgem.jeesite.modules', 'test', null, '树结构生成', '树结构', 'ThinkGem', 'f6e4dafaa72f4c509636484715f33a96', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_scheme` VALUES ('9c9de9db6da743bb899036c6546061ac', '单表', 'curd', 'com.thinkgem.jeesite.modules', 'test', null, '单表生成', '单表', 'ThinkGem', 'aef6f1fc948f4c9ab1c1b780bc471cc2', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_scheme` VALUES ('a6c7059ae58c4fbc81cf7274a19d6b43', 'echartsdemo', 'curd', 'com.zongtui.web.modules', 'echartsdemo', '', 'echarts demo', 'echarts demo', 'zhangfeng', 'f0fa88edce264689825373bdedc91b12', '1', '2015-05-02 14:27:21', '1', '2015-05-02 14:27:21', '', '0');
INSERT INTO `gen_scheme` VALUES ('b3c19bf7c3864436a2d512b83cdc04af', '客户端监控', 'curd', 'com.zongtui.web.modules', 'monitor', '', '客户端监控', '客户端监控', 'zhangfeng', '9a154a0a616042c1b54800acffcd7e15', '1', '2015-05-02 13:45:00', '1', '2015-05-02 13:45:00', '', '0');
INSERT INTO `gen_scheme` VALUES ('e6d905fd236b46d1af581dd32bdfb3b0', '主子表', 'curd_many', 'com.thinkgem.jeesite.modules', 'test', null, '主子表生成', '主子表', 'ThinkGem', '43d6d5acffa14c258340ce6765e46c6f', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) DEFAULT NULL COMMENT '描述',
  `class_name` varchar(100) DEFAULT NULL COMMENT '实体类名称',
  `parent_table` varchar(200) DEFAULT NULL COMMENT '关联父表',
  `parent_table_fk` varchar(100) DEFAULT NULL COMMENT '关联父表外键',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`),
  KEY `gen_table_name` (`name`),
  KEY `gen_table_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('43d6d5acffa14c258340ce6765e46c6f', 'test_data_main', '业务数据表', 'TestDataMain', null, null, '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table` VALUES ('6e05c389f3c6415ea34e55e9dfb28934', 'test_data_child', '业务数据子表', 'TestDataChild', 'test_data_main', 'test_data_main_id', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table` VALUES ('9a154a0a616042c1b54800acffcd7e15', 'monitor_crawler_client', '爬虫客户端监控', 'MonitorCrawlerClient', '', '', '1', '2015-05-02 13:43:22', '1', '2015-05-02 13:43:22', '', '0');
INSERT INTO `gen_table` VALUES ('aef6f1fc948f4c9ab1c1b780bc471cc2', 'test_data', '业务数据表', 'TestData', null, null, '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table` VALUES ('f0fa88edce264689825373bdedc91b12', 'echartsdemo', 'echartsdemo', 'Echartsdemo', '', '', '1', '2015-05-02 14:26:37', '1', '2015-05-02 14:26:37', '', '0');
INSERT INTO `gen_table` VALUES ('f6e4dafaa72f4c509636484715f33a96', 'test_tree', '树结构表', 'TestTree', null, null, '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `gen_table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `comments` varchar(500) DEFAULT NULL COMMENT '描述',
  `jdbc_type` varchar(100) DEFAULT NULL COMMENT '列的数据类型的字节长度',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) DEFAULT NULL COMMENT '查询方式（等于、不等于、大于、小于、范围、左LIKE、右LIKE、左右LIKE）',
  `show_type` varchar(200) DEFAULT NULL COMMENT '字段生成方案（文本框、文本域、下拉框、复选框、单选框、字典选择、人员选择、部门选择、区域选择）',
  `dict_type` varchar(200) DEFAULT NULL COMMENT '字典类型',
  `settings` varchar(2000) DEFAULT NULL COMMENT '其它设置（扩展字段JSON）',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序（升序）',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`),
  KEY `gen_table_column_table_id` (`gen_table_id`),
  KEY `gen_table_column_name` (`name`),
  KEY `gen_table_column_sort` (`sort`),
  KEY `gen_table_column_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('0902a0cb3e8f434280c20e9d771d0658', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '1', '1', '1', '1', '1', '=', 'radiobox', 'sex', null, '6', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('0a0c8c3f5f214c6c91478fa55be37d67', 'f0fa88edce264689825373bdedc91b12', 'name', 'name', 'varchar(50)', 'String', 'name', '0', '1', '1', '1', '1', '1', 'like', 'input', '', null, '20', '1', '2015-05-02 14:26:37', '1', '2015-05-02 14:26:37', null, '0');
INSERT INTO `gen_table_column` VALUES ('103fc05c88ff40639875c2111881996a', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'create_date', '创建时间', 'timestamp(6)', 'java.util.Date', 'createDate', '0', '0', '1', '0', '0', '0', '=', 'dateselect', null, null, '9', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('12fa38dd986e41908f7fefa5839d1220', '6e05c389f3c6415ea34e55e9dfb28934', 'create_by', '创建者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'createBy.id', '0', '0', '1', '0', '0', '0', '=', 'input', null, null, '4', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('195ee9241f954d008fe01625f4adbfef', 'f6e4dafaa72f4c509636484715f33a96', 'create_by', '创建者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'createBy.id', '0', '0', '1', '0', '0', '0', '=', 'input', null, null, '6', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('19c6478b8ff54c60910c2e4fc3d27503', '43d6d5acffa14c258340ce6765e46c6f', 'id', '编号', 'varchar2(64)', 'String', 'id', '1', '0', '1', '0', '0', '0', '=', 'input', null, null, '1', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('1ac6562f753d4e599693840651ab2bf7', '43d6d5acffa14c258340ce6765e46c6f', 'in_date', '加入日期', 'date(7)', 'java.util.Date', 'inDate', '0', '1', '1', '1', '0', '0', '=', 'dateselect', null, null, '7', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('1b8eb55f65284fa6b0a5879b6d8ad3ec', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'in_date', '加入日期', 'date(7)', 'java.util.Date', 'inDate', '0', '1', '1', '1', '0', '1', 'between', 'dateselect', null, null, '7', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('1d5ca4d114be41e99f8dc42a682ba609', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'user_id', '归属用户', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'user.id|name', '0', '1', '1', '1', '1', '1', '=', 'userselect', null, null, '2', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('21756504ffdc487eb167a823f89c0c06', '43d6d5acffa14c258340ce6765e46c6f', 'update_by', '更新者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'updateBy.id', '0', '0', '1', '1', '0', '0', '=', 'input', null, null, '10', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('24bbdc0a555e4412a106ab1c5f03008e', 'f6e4dafaa72f4c509636484715f33a96', 'parent_ids', '所有父级编号', 'varchar2(2000)', 'String', 'parentIds', '0', '0', '1', '1', '0', '0', 'like', 'input', null, null, '3', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('33152ce420904594b3eac796a27f0560', '6e05c389f3c6415ea34e55e9dfb28934', 'id', '编号', 'varchar2(64)', 'String', 'id', '1', '0', '1', '0', '0', '0', '=', 'input', null, null, '1', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('35af241859624a01917ab64c3f4f0813', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'del_flag', '删除标记（0：正常；1：删除）', 'char(1)', 'String', 'delFlag', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', null, '13', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('398b4a03f06940bfb979ca574e1911e3', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'create_by', '创建者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'createBy.id', '0', '0', '1', '0', '0', '0', '=', 'input', null, null, '8', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('3a7cf23ae48a4c849ceb03feffc7a524', '43d6d5acffa14c258340ce6765e46c6f', 'area_id', '归属区域', 'nvarchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.Area', 'area.id|name', '0', '1', '1', '1', '0', '0', '=', 'areaselect', null, null, '4', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('3d9c32865bb44e85af73381df0ffbf3d', '43d6d5acffa14c258340ce6765e46c6f', 'update_date', '更新时间', 'timestamp(6)', 'java.util.Date', 'updateDate', '0', '0', '1', '1', '1', '0', '=', 'dateselect', null, null, '11', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('416c76d2019b4f76a96d8dc3a8faf84c', 'f6e4dafaa72f4c509636484715f33a96', 'update_date', '更新时间', 'timestamp(6)', 'java.util.Date', 'updateDate', '0', '0', '1', '1', '1', '0', '=', 'dateselect', null, null, '9', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('46e6d8283270493687085d29efdecb05', 'f6e4dafaa72f4c509636484715f33a96', 'del_flag', '删除标记（0：正常；1：删除）', 'char(1)', 'String', 'delFlag', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', null, '11', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('4a0a1fff86ca46519477d66b82e01991', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'name', '名称', 'nvarchar2(100)', 'String', 'name', '0', '1', '1', '1', '1', '1', 'like', 'input', null, null, '5', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('4c8ef12cb6924b9ba44048ba9913150b', '43d6d5acffa14c258340ce6765e46c6f', 'create_date', '创建时间', 'timestamp(6)', 'java.util.Date', 'createDate', '0', '0', '1', '0', '0', '0', '=', 'dateselect', null, null, '9', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('53d65a3d306d4fac9e561db9d3c66912', '6e05c389f3c6415ea34e55e9dfb28934', 'del_flag', '删除标记（0：正常；1：删除）', 'char(1)', 'String', 'delFlag', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', null, '9', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('56fa71c0bd7e4132931874e548dc9ba5', '6e05c389f3c6415ea34e55e9dfb28934', 'update_by', '更新者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'updateBy.id', '0', '0', '1', '1', '0', '0', '=', 'input', null, null, '6', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('5a4a1933c9c844fdba99de043dc8205e', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'update_by', '更新者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'updateBy.id', '0', '0', '1', '1', '0', '0', '=', 'input', null, null, '10', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('5e5c69bd3eaa4dcc9743f361f3771c08', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'id', '编号', 'varchar2(64)', 'String', 'id', '1', '0', '1', '0', '0', '0', '=', 'input', null, null, '1', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('6302f16e0e714435999dbd2f7cd72985', '9a154a0a616042c1b54800acffcd7e15', 'crawler_status', 'crawler_status', 'int(11)', 'Integer', 'crawlerStatus', '0', '1', '1', '1', '0', '0', '=', 'input', '', null, '40', '1', '2015-05-02 13:43:22', '1', '2015-05-02 13:43:22', null, '0');
INSERT INTO `gen_table_column` VALUES ('633f5a49ec974c099158e7b3e6bfa930', 'f6e4dafaa72f4c509636484715f33a96', 'name', '名称', 'nvarchar2(100)', 'String', 'name', '0', '0', '1', '1', '1', '1', 'like', 'input', null, null, '4', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('652491500f2641ffa7caf95a93e64d34', '6e05c389f3c6415ea34e55e9dfb28934', 'update_date', '更新时间', 'timestamp(6)', 'java.util.Date', 'updateDate', '0', '0', '1', '1', '1', '0', '=', 'dateselect', null, null, '7', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('669f13d1600b45059ce8a2d1edff0c0b', '9a154a0a616042c1b54800acffcd7e15', 'crawler_name', 'crawler_name', 'varchar(100)', 'String', 'crawlerName', '0', '1', '1', '1', '0', '0', '=', 'input', '', null, '30', '1', '2015-05-02 13:43:22', '1', '2015-05-02 13:43:22', null, '0');
INSERT INTO `gen_table_column` VALUES ('6763ff6dc7cd4c668e76cf9b697d3ff6', 'f6e4dafaa72f4c509636484715f33a96', 'sort', '排序', 'number(10)', 'Integer', 'sort', '0', '0', '1', '1', '1', '0', '=', 'input', null, null, '5', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('67d0331f809a48ee825602659f0778e8', '43d6d5acffa14c258340ce6765e46c6f', 'name', '名称', 'nvarchar2(100)', 'String', 'name', '0', '1', '1', '1', '1', '1', 'like', 'input', null, null, '5', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('68345713bef3445c906f70e68f55de38', '6e05c389f3c6415ea34e55e9dfb28934', 'test_data_main_id', '业务主表', 'varchar2(64)', 'String', 'testDataMain.id', '0', '1', '1', '1', '0', '0', '=', 'input', null, null, '2', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('71ea4bc10d274911b405f3165fc1bb1a', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'area_id', '归属区域', 'nvarchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.Area', 'area.id|name', '0', '1', '1', '1', '1', '1', '=', 'areaselect', null, null, '4', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('7f871058d94c4d9a89084be7c9ce806d', '6e05c389f3c6415ea34e55e9dfb28934', 'remarks', '备注信息', 'nvarchar2(255)', 'String', 'remarks', '0', '1', '1', '1', '1', '0', '=', 'input', null, null, '8', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('8b48774cfe184913b8b5eb17639cf12d', '43d6d5acffa14c258340ce6765e46c6f', 'create_by', '创建者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'createBy.id', '0', '0', '1', '0', '0', '0', '=', 'input', null, null, '8', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('8b7cf0525519474ebe1de9e587eb7067', '6e05c389f3c6415ea34e55e9dfb28934', 'create_date', '创建时间', 'timestamp(6)', 'java.util.Date', 'createDate', '0', '0', '1', '0', '0', '0', '=', 'dateselect', null, null, '5', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('8b9de88df53e485d8ef461c4b1824bc1', '43d6d5acffa14c258340ce6765e46c6f', 'user_id', '归属用户', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'user.id|name', '0', '1', '1', '1', '1', '1', '=', 'userselect', null, null, '2', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('8da38dbe5fe54e9bb1f9682c27fbf403', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'remarks', '备注信息', 'nvarchar2(255)', 'String', 'remarks', '0', '1', '1', '1', '1', '0', '=', 'textarea', null, null, '12', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('92481c16a0b94b0e8bba16c3c54eb1e4', 'f6e4dafaa72f4c509636484715f33a96', 'create_date', '创建时间', 'timestamp(6)', 'java.util.Date', 'createDate', '0', '0', '1', '0', '0', '0', '=', 'dateselect', null, null, '7', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('9a012c1d2f934dbf996679adb7cc827a', 'f6e4dafaa72f4c509636484715f33a96', 'parent_id', '父级编号', 'varchar2(64)', 'This', 'parent.id|name', '0', '0', '1', '1', '0', '0', '=', 'treeselect', null, null, '2', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('ab7b2062e6434057bfebc6b6d3b7d9fe', '9a154a0a616042c1b54800acffcd7e15', 'crawler_id', 'crawler_id', 'varchar(50)', 'String', 'crawlerId', '1', '0', '1', '1', '0', '0', '=', 'input', '', null, '10', '1', '2015-05-02 13:43:22', '1', '2015-05-02 13:43:22', null, '0');
INSERT INTO `gen_table_column` VALUES ('ad3bf0d4b44b4528a5211a66af88f322', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'office_id', '归属部门', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.Office', 'office.id|name', '0', '1', '1', '1', '1', '1', '=', 'officeselect', null, null, '3', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('bb1256a8d1b741f6936d8fed06f45eed', 'f6e4dafaa72f4c509636484715f33a96', 'update_by', '更新者', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.User', 'updateBy.id', '0', '0', '1', '1', '0', '0', '=', 'input', null, null, '8', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('bfe204690c18474ab3311f40347866b1', '9a154a0a616042c1b54800acffcd7e15', 'crawler_ip', 'crawler_ip', 'varchar(50)', 'String', 'crawlerIp', '0', '1', '1', '1', '0', '0', '=', 'input', '', null, '20', '1', '2015-05-02 13:43:22', '1', '2015-05-02 13:43:22', null, '0');
INSERT INTO `gen_table_column` VALUES ('ca68a2d403f0449cbaa1d54198c6f350', '43d6d5acffa14c258340ce6765e46c6f', 'office_id', '归属部门', 'varchar2(64)', 'com.thinkgem.jeesite.modules.modules.sys.entity.Office', 'office.id|name', '0', '1', '1', '1', '0', '0', '=', 'officeselect', null, null, '3', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('cb9c0ec3da26432d9cbac05ede0fd1d0', '43d6d5acffa14c258340ce6765e46c6f', 'remarks', '备注信息', 'nvarchar2(255)', 'String', 'remarks', '0', '1', '1', '1', '1', '0', '=', 'textarea', null, null, '12', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('cfcfa06ea61749c9b4c4dbc507e0e580', 'f6e4dafaa72f4c509636484715f33a96', 'id', '编号', 'varchar2(64)', 'String', 'id', '1', '0', '1', '0', '0', '0', '=', 'input', null, null, '1', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('d5c2d932ae904aa8a9f9ef34cd36fb0b', '43d6d5acffa14c258340ce6765e46c6f', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '1', '1', '1', '0', '1', '=', 'select', 'sex', null, '6', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('e64050a2ebf041faa16f12dda5dcf784', '6e05c389f3c6415ea34e55e9dfb28934', 'name', '名称', 'nvarchar2(100)', 'String', 'name', '0', '1', '1', '1', '1', '1', 'like', 'input', null, null, '3', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('e8d11127952d4aa288bb3901fc83127f', '43d6d5acffa14c258340ce6765e46c6f', 'del_flag', '删除标记（0：正常；1：删除）', 'char(1)', 'String', 'delFlag', '0', '0', '1', '0', '0', '0', '=', 'radiobox', 'del_flag', null, '13', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('eabaeced1f054d40a9a93245a7511f73', 'f0fa88edce264689825373bdedc91b12', 'id', 'id', 'int(11)', 'Integer', 'id', '1', '0', '1', '0', '0', '0', '=', 'input', '', null, '10', '1', '2015-05-02 14:26:37', '1', '2015-05-02 14:26:37', null, '0');
INSERT INTO `gen_table_column` VALUES ('eb2e5afd13f147a990d30e68e7f64e12', 'aef6f1fc948f4c9ab1c1b780bc471cc2', 'update_date', '更新时间', 'timestamp(6)', 'java.util.Date', 'updateDate', '0', '0', '1', '1', '1', '0', '=', 'dateselect', null, null, '11', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');
INSERT INTO `gen_table_column` VALUES ('f5ed8c82bad0413fbfcccefa95931358', 'f6e4dafaa72f4c509636484715f33a96', 'remarks', '备注信息', 'nvarchar2(255)', 'String', 'remarks', '0', '1', '1', '1', '1', '0', '=', 'textarea', null, null, '10', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', null, '0');

-- ----------------------------
-- Table structure for `gen_template`
-- ----------------------------
DROP TABLE IF EXISTS `gen_template`;
CREATE TABLE `gen_template` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `category` varchar(2000) DEFAULT NULL COMMENT '分类',
  `file_path` varchar(500) DEFAULT NULL COMMENT '生成文件路径',
  `file_name` varchar(200) DEFAULT NULL COMMENT '生成文件名',
  `content` text COMMENT '内容',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '更新者',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记（0：正常；1：删除）',
  PRIMARY KEY (`id`),
  KEY `gen_template_del_falg` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代码模板表';

-- ----------------------------
-- Records of gen_template
-- ----------------------------

-- ----------------------------
-- Table structure for `monitor_crawler_client`
-- ----------------------------
DROP TABLE IF EXISTS `monitor_crawler_client`;
CREATE TABLE `monitor_crawler_client` (
  `crawler_id` varchar(50) NOT NULL DEFAULT '',
  `crawler_ip` varchar(50) DEFAULT NULL,
  `crawler_name` varchar(100) DEFAULT NULL,
  `crawler_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`crawler_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of monitor_crawler_client
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_area`
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `type` char(1) DEFAULT NULL COMMENT '区域类型',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_area_parent_id` (`parent_id`),
  KEY `sys_area_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('1', '0', '0,', '中国', '10', '100000', '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('2', '1', '0,1,', '山东省', '20', '110000', '2', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('3', '2', '0,1,2,', '济南市', '30', '110101', '3', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('4', '3', '0,1,2,3,', '历城区', '40', '110102', '4', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('5', '3', '0,1,2,3,', '历下区', '50', '110104', '4', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('6', '3', '0,1,2,3,', '高新区', '60', '110105', '4', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');

-- ----------------------------
-- Table structure for `sys_dict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `value` varchar(100) NOT NULL COMMENT '数据值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `description` varchar(100) NOT NULL COMMENT '描述',
  `sort` decimal(10,0) NOT NULL COMMENT '排序（升序）',
  `parent_id` varchar(64) DEFAULT '0' COMMENT '父级编号',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`),
  KEY `sys_dict_label` (`label`),
  KEY `sys_dict_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '0', '正常', 'del_flag', '删除标记', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('10', 'yellow', '黄色', 'color', '颜色值', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('100', 'java.util.Date', 'Date', 'gen_java_type', 'Java类型', '50', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('101', 'com.thinkgem.jeesite.modules.sys.entity.User', 'User', 'gen_java_type', 'Java类型', '60', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('102', 'com.thinkgem.jeesite.modules.sys.entity.Office', 'Office', 'gen_java_type', 'Java类型', '70', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('103', 'com.thinkgem.jeesite.modules.sys.entity.Area', 'Area', 'gen_java_type', 'Java类型', '80', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('104', 'Custom', 'Custom', 'gen_java_type', 'Java类型', '90', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('105', '1', '会议通告', 'oa_notify_type', '通知通告类型', '10', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('106', '2', '奖惩通告', 'oa_notify_type', '通知通告类型', '20', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('107', '3', '活动通告', 'oa_notify_type', '通知通告类型', '30', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('108', '0', '草稿', 'oa_notify_status', '通知通告状态', '10', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('109', '1', '发布', 'oa_notify_status', '通知通告状态', '20', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('11', 'orange', '橙色', 'color', '颜色值', '50', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('110', '0', '未读', 'oa_notify_read', '通知通告状态', '10', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('111', '1', '已读', 'oa_notify_read', '通知通告状态', '20', '0', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('12', 'default', '默认主题', 'theme', '主题方案', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('13', 'cerulean', '天蓝主题', 'theme', '主题方案', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('14', 'readable', '橙色主题', 'theme', '主题方案', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('15', 'united', '红色主题', 'theme', '主题方案', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('16', 'flat', 'Flat主题', 'theme', '主题方案', '60', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('17', '1', '国家', 'sys_area_type', '区域类型', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('18', '2', '省份、直辖市', 'sys_area_type', '区域类型', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('19', '3', '地市', 'sys_area_type', '区域类型', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('2', '1', '删除', 'del_flag', '删除标记', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('20', '4', '区县', 'sys_area_type', '区域类型', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('21', '1', '公司', 'sys_office_type', '机构类型', '60', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('22', '2', '部门', 'sys_office_type', '机构类型', '70', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('23', '3', '小组', 'sys_office_type', '机构类型', '80', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('24', '4', '其它', 'sys_office_type', '机构类型', '90', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('25', '1', '综合部', 'sys_office_common', '快捷通用部门', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('26', '2', '开发部', 'sys_office_common', '快捷通用部门', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('27', '3', '人力部', 'sys_office_common', '快捷通用部门', '50', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('28', '1', '一级', 'sys_office_grade', '机构等级', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('29', '2', '二级', 'sys_office_grade', '机构等级', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('3', '1', '显示', 'show_hide', '显示/隐藏', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('30', '3', '三级', 'sys_office_grade', '机构等级', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('31', '4', '四级', 'sys_office_grade', '机构等级', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('32', '1', '所有数据', 'sys_data_scope', '数据范围', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('33', '2', '所在公司及以下数据', 'sys_data_scope', '数据范围', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('34', '3', '所在公司数据', 'sys_data_scope', '数据范围', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('35', '4', '所在部门及以下数据', 'sys_data_scope', '数据范围', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('36', '5', '所在部门数据', 'sys_data_scope', '数据范围', '50', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('37', '8', '仅本人数据', 'sys_data_scope', '数据范围', '90', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('38', '9', '按明细设置', 'sys_data_scope', '数据范围', '100', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('39', '1', '系统管理', 'sys_user_type', '用户类型', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('4', '0', '隐藏', 'show_hide', '显示/隐藏', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('40', '2', '部门经理', 'sys_user_type', '用户类型', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('41', '3', '普通用户', 'sys_user_type', '用户类型', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('42', 'basic', '基础主题', 'cms_theme', '站点主题', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('43', 'blue', '蓝色主题', 'cms_theme', '站点主题', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('44', 'red', '红色主题', 'cms_theme', '站点主题', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('45', 'article', '文章模型', 'cms_module', '栏目模型', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('46', 'picture', '图片模型', 'cms_module', '栏目模型', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('47', 'download', '下载模型', 'cms_module', '栏目模型', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('48', 'link', '链接模型', 'cms_module', '栏目模型', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('49', 'special', '专题模型', 'cms_module', '栏目模型', '50', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('5', '1', '是', 'yes_no', '是/否', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('50', '0', '默认展现方式', 'cms_show_modes', '展现方式', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('51', '1', '首栏目内容列表', 'cms_show_modes', '展现方式', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('52', '2', '栏目第一条内容', 'cms_show_modes', '展现方式', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('53', '0', '发布', 'cms_del_flag', '内容状态', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('54', '1', '删除', 'cms_del_flag', '内容状态', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('55', '2', '审核', 'cms_del_flag', '内容状态', '15', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('56', '1', '首页焦点图', 'cms_posid', '推荐位', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('57', '2', '栏目页文章推荐', 'cms_posid', '推荐位', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('58', '1', '咨询', 'cms_guestbook', '留言板分类', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('59', '2', '建议', 'cms_guestbook', '留言板分类', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('6', '0', '否', 'yes_no', '是/否', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('60', '3', '投诉', 'cms_guestbook', '留言板分类', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('61', '4', '其它', 'cms_guestbook', '留言板分类', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('62', '1', '公休', 'oa_leave_type', '请假类型', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('63', '2', '病假', 'oa_leave_type', '请假类型', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('64', '3', '事假', 'oa_leave_type', '请假类型', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('65', '4', '调休', 'oa_leave_type', '请假类型', '40', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('66', '5', '婚假', 'oa_leave_type', '请假类型', '60', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('67', '1', '接入日志', 'sys_log_type', '日志类型', '30', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('68', '2', '异常日志', 'sys_log_type', '日志类型', '40', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('69', 'leave', '请假流程', 'act_type', '流程类型', '10', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('7', 'red', '红色', 'color', '颜色值', '10', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('70', 'test_audit', '审批测试流程', 'act_type', '流程类型', '20', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('71', '1', '分类1', 'act_category', '流程分类', '10', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('72', '2', '分类2', 'act_category', '流程分类', '20', '0', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('73', 'crud', '增删改查', 'gen_category', '代码生成分类', '10', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('74', 'crud_many', '增删改查（包含从表）', 'gen_category', '代码生成分类', '20', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('75', 'tree', '树结构', 'gen_category', '代码生成分类', '30', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('76', '=', '=', 'gen_query_type', '查询方式', '10', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('77', '!=', '!=', 'gen_query_type', '查询方式', '20', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('78', '&gt;', '&gt;', 'gen_query_type', '查询方式', '30', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('79', '&lt;', '&lt;', 'gen_query_type', '查询方式', '40', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('8', 'green', '绿色', 'color', '颜色值', '20', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('80', 'between', 'Between', 'gen_query_type', '查询方式', '50', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('81', 'like', 'Like', 'gen_query_type', '查询方式', '60', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('82', 'left_like', 'Left Like', 'gen_query_type', '查询方式', '70', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('83', 'right_like', 'Right Like', 'gen_query_type', '查询方式', '80', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('84', 'input', '文本框', 'gen_show_type', '字段生成方案', '10', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('85', 'textarea', '文本域', 'gen_show_type', '字段生成方案', '20', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('86', 'select', '下拉框', 'gen_show_type', '字段生成方案', '30', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('87', 'checkbox', '复选框', 'gen_show_type', '字段生成方案', '40', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('88', 'radiobox', '单选框', 'gen_show_type', '字段生成方案', '50', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('89', 'dateselect', '日期选择', 'gen_show_type', '字段生成方案', '60', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('9', 'blue', '蓝色', 'color', '颜色值', '30', '0', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('90', 'userselect', '人员选择', 'gen_show_type', '字段生成方案', '70', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('91', 'officeselect', '部门选择', 'gen_show_type', '字段生成方案', '80', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('92', 'areaselect', '区域选择', 'gen_show_type', '字段生成方案', '90', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('93', 'String', 'String', 'gen_java_type', 'Java类型', '10', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('94', 'Long', 'Long', 'gen_java_type', 'Java类型', '20', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('95', 'dao', '仅持久层', 'gen_category', '代码生成分类\0\0', '40', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('96', '1', '男', 'sex', '性别', '10', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('97', '2', '女', 'sex', '性别', '20', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '0');
INSERT INTO `sys_dict` VALUES ('98', 'Integer', 'Integer', 'gen_java_type', 'Java类型', '30', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');
INSERT INTO `sys_dict` VALUES ('99', 'Double', 'Double', 'gen_java_type', 'Java类型', '40', '0', '1', '2013-10-28 08:00:00', '1', '2013-10-28 08:00:00', null, '1');

-- ----------------------------
-- Table structure for `sys_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `type` char(1) DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(5) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `exception` text COMMENT '异常信息',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`),
  KEY `sys_log_request_uri` (`request_uri`),
  KEY `sys_log_type` (`type`),
  KEY `sys_log_create_date` (`create_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('0061e09922b4449097fc335212544afe', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:45:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('007fcad21a814c3aac3636857b1ed9df', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:31:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('00b09e24eeaa4b988fbb25a8dd665148', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:32:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('0162d788ec2f48f2b49e878f5c5dba4b', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:41:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('016de9fbfa52400eb7a34e72a0d6e63a', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:46:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('017f9574129b40009fa4dd09ce53d195', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:31:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('019c1d9052834322acee156a651afb89', '1', '系统设置-机构用户-用户管理', '1', '2015-05-03 21:27:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('01d8ee06d71a42a1b6419839fc51d55f', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:41:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('021d7137f9b74e539a50e24dddf6e5a2', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:33:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('02c33d4ae9c240a6aced049db93cd7c1', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:15:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('032cde307ab54baaaf07d9299c4d9d0a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:53:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=30', '');
INSERT INTO `sys_log` VALUES ('0369d95736bc4d1891d6aaffe27ee701', '1', '在线办公-通知通告-我的通告', '1', '2015-04-19 23:34:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify/self', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('03d6bc999cb84b7697dbe47f936780c8', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:55:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('04cd922fd02e4eb79e72f0e3f4fd82f4', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 14:34:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('050d407bbbc34332bfc49e78b3c298c9', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:14:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('05acd7c0fd8941eda0943794992ed92c', '1', '系统登录', '1', '2015-05-02 14:18:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('05d3973f6b754b47a573a49adb1ae705', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-17 23:36:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('05f3762c9cd743168618def876fb4204', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('065c8c46979948d28540358082425c7c', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 14:24:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=300&parent.id=1&name=echarts&target=&permission=&remarks=&href=&parent.name=功能菜单&isShow=1', '');
INSERT INTO `sys_log` VALUES ('06f9bb945bb4412bb236225ab4184371', '1', '在线办公-个人办公-审批测试', '1', '2015-04-18 19:02:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/testAudit', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('070e16101fb24f37a2bb2471ddc04db8', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:34:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('07228f03ede44a95943f90afbfb41eca', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:34:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('07282376fcb345aca13baa597c2aaa2d', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:15:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('076db53125d343dd99db87c0aa672044', '1', 'echarts-bar-bar9演示', '1', '2015-05-03 21:42:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar9', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('07fd9408ff6645e2a2eaa18734637938', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:35:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('083136d8d2414273afebbed08be22520', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:49:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=200&parent.id=1&name=监控&target=&permission=&remarks=&href=&parent.name=功能菜单&isShow=1', '');
INSERT INTO `sys_log` VALUES ('08337f10260440378faa6756a71e191f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:49:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('085d783b8ed74ae1a84d25974cf384e9', '1', '系统登录', '1', '2015-05-02 20:58:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('0871a5abce064ce2976c427961ac1b9b', '1', 'echarts-bar-bar9演示', '1', '2015-05-03 21:35:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar9', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('0894495060144907944bfd98224e1982', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:31:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('0961df6e0d914123a34c922f088ba217', '1', '系统登录', '1', '2015-05-03 21:27:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('0988741aad4b4acdb16c3b6d4da38f05', '1', '系统登录', '1', '2015-05-02 13:48:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('09acf9c0ab1c4cc286f483e251bb681a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:28:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('09b77731da5740fba09503f8c1001359', '1', 'echarts-bar-bar5演示', '1', '2015-05-03 21:47:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar5', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('09bd0492ae034ea19e1e3cd569a5925a', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:57:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('09c82a185f384c0887be80d56abc1886', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:33:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('0a3e0748ce2e4d7291092cae92aed22a', '1', '在线办公-个人办公-我的任务', '1', '2015-04-18 19:02:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/task/todo/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('0a57850de72d414592f53e73986fdafd', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:06:44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('0a6745acfe8a423f853895e39ef0aa5e', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:32:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('0a8fac7c44d2477a88372ff29ffc01cc', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:32:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('0b4faf1e7384421da03450d9355ca2bd', '1', '内容管理-栏目设置-栏目管理', '1', '2015-04-18 19:03:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/category/', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('0c9542766f934e309e0e1e6590caba99', '1', 'echarts-bar-bar6演示', '1', '2015-05-03 21:47:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar6', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('0cdb57dfd9644ddab28004c54d32823a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:11:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('0cece7fd462b4e57ab1e0a5c8111e2a3', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 14:34:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('0d129d37d957422bac462a2103870737', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 14:18:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('0d90dd27c1ed4464bea015b67770f658', '1', '代码生成-生成示例-树结构-查看', '1', '2015-05-02 13:07:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('0da372bcd5e14401842a41d8a55e65da', '1', 'echarts-bar-bar3演示', '1', '2015-05-03 21:34:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar3', 'GET', 'tabPageId=jerichotabiframe_21', '');
INSERT INTO `sys_log` VALUES ('0dba6abd9b3d4d7ca449c3e3cc530c67', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:30:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('0e014504a2f34686b554146f1e363d9e', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-03 21:34:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('0e388e750c2d42ffbbaa21451d06b458', '1', '系统登录', '1', '2015-04-21 22:13:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('0eb390d978fc45cb9cc22b6d03ce4070', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:46:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('0eb5bbbae7004dadacdcfe2aa0f39f1b', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-03 21:27:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('0f36133c2af042aebd47da10fcb6efe0', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('0f418225df984f2c9683553f68347044', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 13:06:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('0fb1fb069a0141b1a779810e8b9e4076', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:36:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('1008e36331f3439d9082686bf903c7bb', '1', '代码生成-生成示例-主子表-查看', '1', '2015-05-02 13:07:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testDataMain/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1037854dbb2f4b9183f4152fef979af0', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:13:13', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('106b27542d6b4bf1b41f39a42f6935cd', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:53:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('107020a9a5444d2282ed5381916c301d', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('10959261ca6b4624b9181611dd1dcb19', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('10fbb1c97df54539a6169b81c8f67d75', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:35:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('1103e69e25d04ce7ac870ec3fd91d9c2', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:30:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('1107f13c6c2e410386fc98ad108f366f', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('11af118b99ed41e58d2e32806030b990', '1', 'echarts-line-line演示', '1', '2015-05-02 21:32:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('11c36fc9c7834c93a3f4f3eae33f13d0', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:36:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('11d3ad01d7f0484b9f621b19ebc2acc8', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:49:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('12a9732d82cf4134b6a5665540865ead', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:14:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('134687116ea640f28a9b1ee6b45acf82', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:15:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('140d2b9a8d7c4c4c927a9971d6e3131c', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('14a09988853341ab82a7ad73d0286512', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:04:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=20&parent.id=06fc54514cdf45d992c1f5403215d016&name=bar&target=&permission=&remarks=&href=&parent.name=echarts&isShow=1', '');
INSERT INTO `sys_log` VALUES ('14b9fe20f053476e816345dcffbc8028', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 20:57:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('1518e6e5860940c3bbcbddb84159f0b8', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:28:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=60&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar1演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar1&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('152649b377b54f049518fa7cd88c45b6', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:34:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('1598931d6c2546788d33d9008f24b8eb', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:32:32', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('15b5300716234d029c2bc96307276ff3', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:35:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('15f2f15ff0e34145aed8349c870fd3b6', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:35:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_35', '');
INSERT INTO `sys_log` VALUES ('170cfdf234fb43088f6b42a8d9fa7ab0', '1', '代码生成-代码生成-业务表配置', '1', '2015-04-21 22:14:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_17', '');
INSERT INTO `sys_log` VALUES ('172323478f7d4325808ceb1b78d8e3e6', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 14:23:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1735e966cd59407698ed33d840e58211', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:39:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('173d4bc56b744baaa028b3514d053f40', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:45:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('17f328a0fc53467c8ccdfde3a0c19b30', '1', 'echarts-bar-bar3演示', '1', '2015-05-03 21:47:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar3', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('180a573f327c49389d4c721a504379ea', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:00:26', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('18251424a0394f5cbeb3e2074104274d', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 13:53:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1896b3bbd08e438482659754f12c91c0', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 20:45:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=1948167a2d504222b470899fbd139ec9', '');
INSERT INTO `sys_log` VALUES ('18a260c926144a058f88ff105cd9f4aa', '1', '代码生成-代码生成-业务表配置', '1', '2015-05-02 14:26:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('19abb452f9fe43dfbc632ed66712d287', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:00:42', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('19dc65d994524b0584f81fecb7996716', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 20:45:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1a9877a700624cc89d2381e09634b12b', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:48:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('1aaf4b935b464c0083a8c51ec48553b9', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('1b6f042cb27a4b798f1e9e70abe61e54', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:31:13', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=06fc54514cdf45d992c1f5403215d016', '');
INSERT INTO `sys_log` VALUES ('1ba907c66aaa4ad9a503d49f3c9f065c', '1', '内容管理-内容管理', '1', '2015-04-18 19:03:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/tree', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1bca340d606044b58d15436e1b6c4de6', '1', '系统登录', '1', '2015-05-02 13:41:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('1c219b66b00f406a9791c64a9c543628', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:29:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1c3e1cc756694187bdc6c22b7203ac5e', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:49:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1cfb2e2146ad402fb57e15cebd0ce97f', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:29:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1d08796a890a4a688c6eaf8e1593cb3a', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 15:09:48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1d14122874944d61bab82f7a05ef347d', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:51:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1d361d3f24c44ece80908a63edf6fc7d', '1', '系统设置-系统设置-菜单管理', '1', '2015-04-19 23:34:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('1d8923906bc94d9da49c0f73212d3fa0', '1', '系统登录', '1', '2015-05-02 15:06:43', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('1dc67ab3c00c4104b6e70473fb536dd7', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 21:14:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('1de6c04813e0499f947da584ea9481a9', '1', 'echarts-bar-bar6演示', '1', '2015-05-03 21:35:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar6', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('1ee1950d190c4e408b4ecd053382d628', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:36:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('1ef4d8fc541749d483561f6e0536bc30', '1', '代码生成-代码生成-生成方案配置', '1', '2015-05-02 14:26:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genScheme', 'GET', 'tabPageId=jerichotabiframe_16', '');
INSERT INTO `sys_log` VALUES ('1f292152425c47b880690fc5a150eea1', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:16:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('1f916e24d9eb42d7a43a86946febe4f6', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:34:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('1fc163633f7444459a60fc36a6022d5a', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:36:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('1fee255473d744bcba641ea4e0b6a4b6', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:12:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('208f86a528e0482aa50dd820ccaccd18', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:28:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('20ce8ad4474448858b78a66393846b0c', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('20fe972850a8416da928e6e635c6678c', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:49:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('21a54e1bcb4e4e739d655f01bb7c66e0', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:34:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('21fc4623a66746d1954347b9bf442aaa', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 13:52:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('22a19526fa1b4227af917ef54abbf5f8', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:15:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('22d9feaf6c964357b2401dec60cc0232', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('246ec7c98c3b4e12a99a2dbd1c2f776d', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:47:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('24c9c3f5dba64a91b05bb04273c27068', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:09:25', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('24ff843ae692497ca7e6bdccefc469c7', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:32:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('25129bd0e1634c7cbd142f62e9cd55c2', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:05:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('252277e5ea0b4b7b9a252ed262e32d65', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:44:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('25917dbf3f0240e7bf3d629ae55d0fdb', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:46:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('25d1bb3f360a4a7d9e74270978f5ff77', '1', 'echarts-bar-bar1演示', '1', '2015-05-03 21:33:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('25e4c727a7d94d87baf99480a6bf5b04', '1', '系统登录', '1', '2015-05-02 14:38:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('25eb4af9b0204d96892470eb4f3fb732', '1', 'echarts-bar-bar14演示', '1', '2015-05-03 21:34:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar14', 'GET', 'tabPageId=jerichotabiframe_31', '');
INSERT INTO `sys_log` VALUES ('2605a56692ec4becb39c6cf2b9cf1d87', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:54:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('264116cbd6864a8c8401cc397dcfeb7c', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:07:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('2796938362c04eb88f8e5f4f28c71b6e', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 21:14:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('27a07a2b28434d659bbce633278173d4', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:45:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('27e39dfecb6d4e13a04a22d388beda45', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:11:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('27ee15cb9cf44c04a5bb34b2c318fe4b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:51:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('2847b205d733465baaa72643d2a02824', '1', '代码生成-代码生成-业务表配置', '1', '2015-05-02 13:07:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('28973f70ab8d41c48a6c134e4ebf4172', '1', 'echarts-bar-bar9演示', '1', '2015-05-03 21:48:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar9', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('29833ba3e592470d84c056f039e6311d', '1', '系统登录', '1', '2015-05-02 14:34:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('29a10858b7b94fb4b23f500d13538b8e', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:15:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('2a2f4c0d574a46f1b4b2165c784c8338', '1', '系统设置-机构用户-用户管理', '1', '2015-04-19 23:34:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('2a46977642394e1089f298ba91fe1968', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:11:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('2a8039da1e0c4c20b38cc7b9fec9429a', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-04-21 22:13:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/list', 'GET', 'id=&parentIds=', '');
INSERT INTO `sys_log` VALUES ('2b7084c5ff0f4dfaaf804d26a87b8b56', '1', '系统登录', '1', '2015-05-02 15:42:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('2bb8e1c49c5548fa9080bdaebe9f70b4', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 20:46:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('2bf9fe8c42fa42cc93674b7af4417f84', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:00:42', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=18be3c3752744cf4b8be4f686e6b2586&icon=&sort=50&parent.id=24ebfed0acae410a86c81628d2659a67&name=line1演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line1&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('2c4835955f854704ba9c78c7b236e2fc', '1', 'echarts-bar-bar11演示', '1', '2015-05-03 21:34:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar11', 'GET', 'tabPageId=jerichotabiframe_29', '');
INSERT INTO `sys_log` VALUES ('2ce4b1ca428f41b4b946540b219c6f18', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:02:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('2d0586c848324bb38f401788d60c9c3e', '1', '我的面板-个人信息-修改密码', '1', '2015-04-21 22:13:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('2d4f6dfb5e5d41daa9e4139a257d48cc', '1', '系统设置-系统设置-角色管理', '1', '2015-05-03 21:33:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('2dc5d8be2d514da78a83d7ae750f177c', '1', '系统登录', '1', '2015-05-02 13:55:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('2df583a4b67e4abe8c515cd2fdf13022', '1', '我的面板-个人信息-个人信息', '1', '2015-04-18 19:01:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('2e21ea3969474ffbab43b2162a16d041', '1', '系统设置-机构用户-机构管理', '1', '2015-05-02 14:18:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('2f46742eb63349f5a9e0fac4636adc62', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:14:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=260&parent.id=24ebfed0acae410a86c81628d2659a67&name=line10演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line10&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('2f5377c37b0e42a2b850c22f65ccc51d', '1', '系统设置-机构用户-机构管理', '1', '2015-04-21 21:46:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('2f98d9a0749244ffba193d9bef64a53c', '1', '在线办公-通知通告-我的通告', '1', '2015-04-18 19:01:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify/self', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('302c8e7cba494d848ab12bc992199843', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:35:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('303dae2eeeb142d5a7338e54f090f8f9', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-05-02 14:18:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('30e319cdbd424fa68168f990cc8f7a85', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 21:00:24', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('31a1fefc5f854bdb88d3dd7e8a7e2726', '1', 'echarts-bar-bar15演示', '1', '2015-05-03 21:37:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar15', 'GET', 'tabPageId=jerichotabiframe_22', '');
INSERT INTO `sys_log` VALUES ('31cf0dd119cf462db0ea7627ca4f3440', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:34:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('336612f90c404dc9a4cd5ef09379e7b9', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 21:02:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('339c2e6b520443e18284dbfa4b424566', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:50:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('33a6a629552e42229c302638c4af8a24', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('33aacaa68cfc44aca0fbf9dacf0f0798', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 20:56:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('33bb17a0703e4b2a803fb3894fa990c2', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:34:13', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('33f5cc17eeae4ed0a358dc22de74a977', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 20:56:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('34619c4f768442ea825b45392b2133b6', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:39:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('34b5439f33294b05812f37cc9c9d95b1', '1', 'echarts-line-line演示', '1', '2015-05-02 21:32:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('34ba805bb84248a4b7a7dd4908274d4c', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 21:14:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('34dafde8a75b4ec19cc7d57a89a8a05b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:45:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('35293d18a1cd46f2bd13be65539a625d', '1', 'echarts-bar-bar9演示', '1', '2015-05-03 21:34:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar9', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('354cbeaf6bb74a36a515f059dfa366aa', '1', 'echarts-bar-bar4演示', '1', '2015-05-03 21:35:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar4', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('36367fc3e4874f65b6b6455b41c06a04', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:31:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('365399faffa84cedb3e946172f7226d0', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:14:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('366a8c4eec9547c191c10b0d461f47dc', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 15:09:47', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=1948167a2d504222b470899fbd139ec9&icon=&sort=30&parent.id=24ebfed0acae410a86c81628d2659a67&name=line演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('3684672e594c408d8c828dd1599c04b1', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:04:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('368639c157684741bf2dbe26d00e1875', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('36a6253e46924c2fa539f5a80cbbad46', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:36:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('36c12d58ab4646498f0aac56b9a0e0a0', '1', '我的面板-个人信息-修改密码', '1', '2015-05-02 14:18:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('3705e6c2a5e74df2aa718b433cf47825', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('370c7934bf35488face274d0b97997de', '1', '系统登录', '1', '2015-05-02 15:38:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('37c5f08ab79c4852a56faf3a47e3638b', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:32:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('37c76079057644719d16bf5338363ef5', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:03:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=06fc54514cdf45d992c1f5403215d016', '');
INSERT INTO `sys_log` VALUES ('387d21b885a648899cf0b51770dec833', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:45:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('3884705da4214701b064282eb300708d', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:03:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('3898e8049f294289b9c6b70f035d627a', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:36:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('38d0c2404b364183a3ccbae6239adfd3', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 21:02:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('38d9742dc72348fb98839bb60817d77d', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:08:47', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('396e9026dddb435181b27df62b24f813', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:40:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('39d2e84055e34ec795ecefb8fc16e262', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:13:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('39dc4b5b142b44e2961581b7beb21f64', '1', 'echarts-bar-bar14演示', '1', '2015-05-03 21:48:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar14', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('3a31046e73834223a9d17d6d32dc0034', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:41:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('3a314e3fe2d94f6c84afaac4e70682d8', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:14:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('3a3ed029e063415dbd51c161de823e4b', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:43:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('3b407d420551497e843ad71251a84c21', '1', '代码生成-生成示例-单表-查看', '1', '2015-05-02 13:45:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testData/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('3b5caada6e794dc8bbf9a26e861288fc', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 20:45:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=1948167a2d504222b470899fbd139ec9', '');
INSERT INTO `sys_log` VALUES ('3b8aa380dfe24eff8d1cc80d768e9223', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:34:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_34', '');
INSERT INTO `sys_log` VALUES ('3bbce9f75db84582b4fcf2db009c5e2b', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:53:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('3c6d126afd0c481f8fce7199ed52fa4a', '1', '我的面板-个人信息-个人信息', '1', '2015-04-19 23:34:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('3c6dfa836165446884d3a73f1dd6f2c5', '1', '系统登录', '1', '2015-04-17 23:36:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('3c7345a04a4b49c69425fb6a9dd269cd', '1', 'echarts-line-line演示', '1', '2015-05-02 21:32:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('3d037a5cdee14576be4d5f79714845dc', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:46:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('3d305b5cbdc4472ea9ae1fa4167c503e', '1', '系统登录', '1', '2015-04-19 23:34:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('3d38bd428bf54dd7b49343b17550613f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:20:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('3d8e5259bce649c0a7b98aea730bc8a9', '1', 'echarts-bar-bar9演示', '1', '2015-05-03 21:34:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar9', 'GET', 'tabPageId=jerichotabiframe_27', '');
INSERT INTO `sys_log` VALUES ('3dfaad28ebb04f8bb2b8b0f5f89e41ff', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:30:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=240&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar7演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar7&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('3e29f69717e24c9f863e09232b11ad71', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 14:34:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('3f37889171f94af6b82f4321a600ea39', '1', '在线办公-通知通告-通告管理', '1', '2015-04-18 19:01:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('3ff9aba004db45b98e4b71d9daf754ee', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('407f26042efe45a8ab61c95982f6fdd1', '1', '代码生成-生成示例-单表-查看', '1', '2015-05-02 13:45:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testData/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('40bdea8b3d0342258be4be9d86958d95', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:40:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('40e8120f35814acdbe085b701c7f9398', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:11:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=60&parent.id=24ebfed0acae410a86c81628d2659a67&name=line2演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line2&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('414f5ac992e64a7594e582fd83eadd4e', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 15:07:03', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=1948167a2d504222b470899fbd139ec9&icon=&sort=30&parent.id=24ebfed0acae410a86c81628d2659a67&name=axis演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('41741d55e58d4c30a301db1ec808bbdc', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:35:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('4233b8d2314645aab26925a31dc0235e', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:56:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('429c26be22ad4cfe9cca458ae16ab2a1', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:33:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('42b9264e58db45d1b30c4f1b22e70864', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 13:59:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('43422b2238894608823a159d7683a686', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:36:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('43518d3971d2430999836810daa71f41', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 20:58:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('437a906c4e9747b9ab3a61c931dca67e', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:25:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('43c5566505c24387b85239665c70b700', '1', 'echarts-bar-bar1演示', '1', '2015-05-03 21:35:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('43ea0d680f044c578d9228e771128591', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:43:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('4427903d918e4afaa447f00abd9f03d9', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:12:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('4463e15a75a541e98b38ff5631cd4378', '1', '我的面板-个人信息-修改密码', '1', '2015-04-18 19:01:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('4517d664a56a4e86a9e31d63e47b54bd', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:02:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('453525b76a284b8691c72f58429fb6ef', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('456b577538ca4c0fbccc280aaefe2c60', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:03:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=24ebfed0acae410a86c81628d2659a67&icon=&sort=30&parent.id=06fc54514cdf45d992c1f5403215d016&name=line&target=&permission=&remarks=&href=&parent.name=echarts&isShow=1', '');
INSERT INTO `sys_log` VALUES ('456e4574140d4b9c80d8b30d8128415b', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:36:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('45d3e614291842e588e0c76e9b4b4d0c', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:34:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_20', '');
INSERT INTO `sys_log` VALUES ('460fedb97e39449d82fd9598e708600e', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:15:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('467fc3adb3804e3a9a49c089d322c6be', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('46aeae12109f4689b5d045c33a122b95', '1', 'echarts-bar-bar4演示', '1', '2015-05-03 21:33:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar4', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('46b3686fadf04876aa9c522e53c266b8', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:55:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('46be7598b692462ea545da38e7543242', '1', '代码生成-代码生成-生成方案配置', '1', '2015-05-02 13:07:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genScheme', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('47bde686c177483b9a214bb9a5597d50', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:28:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=90&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar2演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar2&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('47dac11801864209b520b37d378e8ed9', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:07:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('48351a5c50674f5390bdb3ac4a11bc5f', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 15:09:33', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('48f7aa828046471f89c3e8df681f1730', '1', '系统设置-机构用户-机构管理', '1', '2015-04-18 19:04:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/', 'GET', 'tabPageId=jerichotabiframe_18', '');
INSERT INTO `sys_log` VALUES ('4a8b233a8ac948888ff1e0e0d59001a0', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:40:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('4ad827d3be22425cb663dbb3c413f175', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:42:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('4b064962ceb646dea8cbe8f9a94e17ed', '1', 'echarts-bar-bar7演示', '1', '2015-05-03 21:47:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar7', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('4b293c2114ac4fff936b35cf2236717a', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 20:45:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('4b5f2e0c946a4f57a0cd1718f89def85', '1', '我的面板-个人信息-个人信息', '1', '2015-04-18 19:01:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('4b67c8c199d140e8949f3ab9988d4039', '1', '在线办公-流程管理-流程管理', '1', '2015-04-21 22:13:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/process', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('4b75f1dbdc624c8697b52968c021d49f', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:35:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('4bbecad709784a669cb142e86dce5282', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:33:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('4bd7f012eabe44e395302d07c75952d6', '1', '系统登录', '1', '2015-05-02 13:52:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('4be176770b8a4dab86aa58671c628320', '1', '代码生成-代码生成-业务表配置', '1', '2015-05-02 13:41:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('4cb1831e430040a49e1b08d47e4355ce', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:29:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=150&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar4演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar4&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('4ce78ffb86654bab98f4a19cf6f2a371', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 21:01:31', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('4cfa19632cd44bf1b5068884dbe9d320', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:51:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('4da9aa77e9cf4ca289d73483a9f0678c', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:59:54', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('4e8b52366810490699f38330009aaa37', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:18:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('4ecc0b988f5b43b199e07fae7ce3f702', '1', '系统设置-日志查询-日志查询', '1', '2015-05-02 14:19:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/log', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('4f0b8fe480e146fbb73218a7ccb70b5f', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:35:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/updateSort', 'POST', 'sorts=100&ids=27', '');
INSERT INTO `sys_log` VALUES ('4f2a3421465c47e8874353b6ab7b2007', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:29:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('4ffaf18bca4c4040a0aefb453bf800de', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 13:06:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('503b55c9a2ab499490ba561f0b23befe', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-21 22:13:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('5067bec5aa9c422ea7c6e98a1af3c894', '1', '代码生成-生成示例-树结构', '1', '2015-04-21 22:14:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree', 'GET', 'tabPageId=jerichotabiframe_21', '');
INSERT INTO `sys_log` VALUES ('50c745b4d880477bb5c9f3dbb923a1e9', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:06:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('514f597a5afc4a1ab322b617e62af7a4', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:32:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('52385bfa6bd248138a98661a720d324c', '1', '系统设置-机构用户-用户管理', '1', '2015-04-21 22:13:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('52c8c4337e4944fa9c359f3306bfad00', '1', '系统设置-机构用户-用户管理', '1', '2015-04-18 19:04:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_17', '');
INSERT INTO `sys_log` VALUES ('531737d19a824239971b68c26584680b', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:12:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=110&parent.id=24ebfed0acae410a86c81628d2659a67&name=line5演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line5&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('533e7f451d1c4899838a35470d26119b', '1', '我的面板-个人信息-个人信息', '1', '2015-04-21 21:46:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('536f80fd25a34414b009f9d1e166e92a', '1', '系统登录', '1', '2015-05-02 13:59:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('5383d810cd4e4cc8af204204a198141c', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:44:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('538b534cc03d4907ac5a10ea1ef98c61', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('540277eb6ec340dcb2cf9dd3c0703333', '1', 'echarts-bar-bar3演示', '1', '2015-05-03 21:42:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar3', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('54c2b07ccedf4ebabbbf5799a18baf70', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:03:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('54f3b0267f0549ff8fe1c8eadacd2c81', '1', '代码生成-代码生成-生成方案配置', '1', '2015-05-02 13:46:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genScheme', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('55ae119b7aa7435a9c5b6df00f2b5393', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:04:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=1948167a2d504222b470899fbd139ec9', '');
INSERT INTO `sys_log` VALUES ('56c2f012114a4fb18c79304d9fcbc10e', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:25:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('56f074bb9617476cb9e43abb382df17b', '1', '系统设置-系统设置-角色管理', '1', '2015-04-21 22:14:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('571a9ead53e64c36a7440866de610828', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:32:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=390&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar13演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar13&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('57b3defcc4f94d06a1f9a574f032a8b1', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:34:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('589cae43d93642f1ae47d9e475ee77dd', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 21:14:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('58bcaa09e3e444b68506dc1b3ea6e5a0', '1', '内容管理-栏目设置-切换站点', '1', '2015-04-18 19:03:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/site/select', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('58d2c758eeb44127aaf6eb8a30ac29a6', '1', 'echarts-line-line演示', '1', '2015-05-02 21:28:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('58d479a04bc94e77960de9ba98dd48c2', '1', 'echarts-bar-bar7演示', '1', '2015-05-03 21:35:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar7', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('59d6ab9a2225435ba2f9253f9f00fba2', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:33:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('5b298e5ad90b4ababd72edd742d1a02f', '1', '系统登录', '1', '2015-05-02 14:59:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('5b56df94f4b3499ab91775c8a78f1e8d', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:03:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/delete', 'GET', 'id=735c0343973d487d8a3731f2f9918cbd', '');
INSERT INTO `sys_log` VALUES ('5b7c4dc4267b40d0918fcad66957f60e', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:13:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('5bfc4b87fd2d4d6ba3f4008180f4eaa5', '1', '系统设置-机构用户-机构管理', '1', '2015-04-21 22:13:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('5c0f31ba32514873b976c1e1c387df31', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 13:48:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('5c6a3daa8466436f8772e279ba4c7e2d', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:06:46', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('5d18824504df4b5e87b9248205aee42b', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:13:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=170&parent.id=24ebfed0acae410a86c81628d2659a67&name=line7演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line7&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('5d8b7d62dd3e4a9b9365f32cabec39f2', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:51:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=66a8428823df40a6b15d1179a4f86758&icon=&sort=30&parent.id=8ec74143ca2d4263a11df0f52307671a&name=爬虫节点监控&target=&permission=&remarks=&href=/monitor/monitorCrawlerClient&parent.name=爬虫监控&isShow=1', '');
INSERT INTO `sys_log` VALUES ('5dda409a188041c99bdd74efbf48eeda', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:21:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('5ea54641a3c34d538ceaba5338c6dcf6', '1', 'echarts-bar-bar15演示', '1', '2015-05-03 21:42:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar15', 'GET', 'tabPageId=jerichotabiframe_16', '');
INSERT INTO `sys_log` VALUES ('5ebdbf1c1aeb45918b3873deb1afb2f2', '1', '系统设置-机构用户-用户管理', '1', '2015-04-17 23:36:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('5ecbd696944947319d0d08cbb11729bf', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:27:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('5ee45778a72d435ea7bdad714d345a94', '1', '系统登录', '1', '2015-05-03 21:33:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('5f0321c79df54cacbe304935db3bd69f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:41:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('606604acc44c4201a071aeabf527437a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:31:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('60ea59f5a1554118811931712ffc7177', '1', '内容管理-统计分析-信息量统计', '1', '2015-04-18 19:03:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/stats/article', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('6160357dd7a94505a3132e83fdfa1583', '1', '系统登录', '1', '2015-05-02 14:33:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('61605e98512d42ef8f6d2ebcbbefc7b6', '1', '我的面板-个人信息-个人信息', '1', '2015-04-17 23:36:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('61bc2e921e29464088419d21c4619fe8', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:10:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('6256345d5f7b4a379d56143c261728c4', '1', 'echarts-bar-bar5演示', '1', '2015-05-03 21:35:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar5', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('627e99ea1068455bb1c50daee7e06ac7', '1', 'echarts-bar-bar4演示', '1', '2015-05-03 21:34:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar4', 'GET', 'tabPageId=jerichotabiframe_22', '');
INSERT INTO `sys_log` VALUES ('62c945e9cea840acbc7694263fc8f30a', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:32:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('62d3391dab7747e3bf66f30d8b00fffd', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:35:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('63109548fc4b46d6ac116a2a5aeb5eb1', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:51:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=30&parent.id=8ec74143ca2d4263a11df0f52307671a&name=爬虫节点监控&target=&permission=&remarks=&href=&parent.name=爬虫监控&isShow=1', '');
INSERT INTO `sys_log` VALUES ('6372c69189f84bb9b9fb68271137f586', '1', 'echarts-bar-bar5演示', '1', '2015-05-03 21:33:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar5', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('6381fe0b26b04bfa9e282b55f5513db7', '1', 'echarts-bar-bar12演示', '1', '2015-05-03 21:36:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar12', 'GET', 'tabPageId=jerichotabiframe_17', '');
INSERT INTO `sys_log` VALUES ('64c961e8ae5744f18cfae9ab2035cdb8', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 20:59:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('64fa7fa4a0954d4fb1b1c6da79d3ab28', '1', 'echarts-bar-bar6演示', '1', '2015-05-03 21:34:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar6', 'GET', 'tabPageId=jerichotabiframe_24', '');
INSERT INTO `sys_log` VALUES ('652c156f8d84478d9e27821b66698f58', '1', '系统登录', '1', '2015-05-02 14:43:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('65411c9134fc4f30be156ccc36189faf', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:36:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('65b7d1355f93413c8daca46fdcf844c0', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:31:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('65b8a472999e42399d35c1198d438779', '1', 'echarts-bar-bar12演示', '1', '2015-05-03 21:34:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar12', 'GET', 'tabPageId=jerichotabiframe_19', '');
INSERT INTO `sys_log` VALUES ('661c30304ac24eb897c733d3ab3095cd', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:46:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('664afd541dfc4d0295432da276b688ff', '1', 'echarts-bar-bar10演示', '1', '2015-05-03 21:34:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar10', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('6732784e620b43908676f54ccd778e06', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 21:01:35', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('67b3f7c0770843e1bb9fa0dbf7df2fe9', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 21:00:24', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('681c0f7c99b7405397ac83ad919a370a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:14:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('684884118f534dbd8f69e84bd0b0b6b9', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:41:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('684bedb6a55c44a9b2e2cb4618839a14', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:32:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('68ccc19745ed4dada5d3ffe6800c5663', '1', '系统设置-系统设置-字典管理', '1', '2015-05-02 14:19:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/dict/', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('6913af2c53e84994aeb4458f0c3b0db8', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-18 19:04:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('6955c13a4c064873ae029c6dcbd58d80', '1', '系统登录', '1', '2015-05-02 21:39:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a;JSESSIONID=37dd60865a374ad8b8c8aba8c0beeae3', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('69bad17de9e94343b4c721d0c63881be', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:51:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=66a8428823df40a6b15d1179a4f86758', '');
INSERT INTO `sys_log` VALUES ('69e96b737e654d51964dfc5ab623799e', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 20:45:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('69ec4d0fbd2b4c3f9e35f05fe9244b3d', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:34:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('6a09911f259d4ec29d7d87cc619a923c', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:21:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('6a624cc417b04aab8bb638b9ed1bf1db', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:28:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('6a6adc0c1f2548f2b617aca4adf16d0f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:35:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('6ae1d7b635b3434b9e5c1f74dd60f820', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:51:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('6b608143d4474621bd5ea3db34978c11', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:15:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('6b92298e2ec14bbe8cca4ab9adc335c2', '1', '我的面板-个人信息-修改密码', '1', '2015-04-18 19:01:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('6bd7c0690d914149bba59c039183708f', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:15:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'GET', 'tabPageId=jerichotabiframe_17', '');
INSERT INTO `sys_log` VALUES ('6c1ed66b299544b09e946ec42ab0e8df', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:50:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('6d7224ffbf584b04964b97bcd796b4ca', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:41:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('6db36ed9eff248819457c417066c8cca', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:52:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('6db7acd669c941519eaf7af23c9a78a0', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:10:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('6dbb3d4ccfec4c3586d81fcb08dc4ddf', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 14:25:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=30&parent.id=06fc54514cdf45d992c1f5403215d016&name=demo演示&target=&permission=&remarks=&href=&parent.name=echarts&isShow=1', '');
INSERT INTO `sys_log` VALUES ('6e052e60c588417ab382155450b71b82', '1', '在线办公-个人办公-审批测试', '1', '2015-05-02 13:06:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/testAudit', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('6e536b6775054f6cb0f790ee92c7cee7', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 20:57:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('6f0bfef68a1b4a1ab0205a995c4c458e', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:07:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('6f50dc41dafd4259854066b8551b38c9', '1', 'echarts-bar-bar6演示', '1', '2015-05-03 21:33:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar6', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('6f603073ac9a4a1588b1903026236440', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 15:09:34', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('6fc25699bcb7400aa3c431c83ac29f58', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-05-02 14:18:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/list', 'GET', 'id=&parentIds=', '');
INSERT INTO `sys_log` VALUES ('70dc80e7de204a6cb028a8e5a0f0c207', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:45:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('710a112776f143c2914975f3b72fbf1d', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 20:45:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=1948167a2d504222b470899fbd139ec9&icon=&sort=30&parent.id=24ebfed0acae410a86c81628d2659a67&name=line演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('71438a25b3aa4ae0913b5083f4f7fb63', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 13:52:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'repage=', '');
INSERT INTO `sys_log` VALUES ('714c9cbf2443419583b5c49f037648bb', '1', 'echarts-bar-bar8演示', '1', '2015-05-03 21:42:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar8', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('7173bd68e02c4e55bcff0b7a1bd4ac34', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('71793a04179e46aaacfbe39907782804', '1', 'echarts-bar-bar7演示', '1', '2015-05-03 21:33:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar7', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('717eb092899a429d8de90bf1f6005127', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:33:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('7180dde4fa164120901ac627ba82ac74', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:28:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('719411053b414eb889aab64099047317', '2', '', '1', '2015-04-19 23:30:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', 'org.apache.jasper.JasperException: PWC6299: The class com.zongtui.web.modules.cms.entity.Site specified in TLD for the function fnc:getCurrentSiteId cannot be found: com.zongtui.web.modules.cms.entity.Site\r\n	at org.apache.jasper.compiler.DefaultErrorHandler.jspError(DefaultErrorHandler.java:89)\r\n	at org.apache.jasper.compiler.ErrorDispatcher.dispatch(ErrorDispatcher.java:375)\r\n	at org.apache.jasper.compiler.ErrorDispatcher.jspError(ErrorDispatcher.java:169)\r\n	at org.apache.jasper.compiler.Validator$ValidateVisitor$1MapperELVisitor.visit(Validator.java:1725)\r\n	at org.apache.jasper.compiler.ELNode$Function.accept(ELNode.java:164)\r\n	at org.apache.jasper.compiler.ELNode$Nodes.visit(ELNode.java:235)\r\n	at org.apache.jasper.compiler.ELNode$Visitor.visit(ELNode.java:302)\r\n	at org.apache.jasper.compiler.ELNode$Root.accept(ELNode.java:95)\r\n	at org.apache.jasper.compiler.ELNode$Nodes.visit(ELNode.java:235)\r\n	at org.apache.jasper.compiler.Validator$ValidateVisitor.getFunctionMapper(Validator.java:1766)\r\n	at org.apache.jasper.compiler.Validator$ValidateVisitor.visit(Validator.java:758)\r\n	at org.apache.jasper.compiler.Node$ELExpression.accept(Node.java:946)\r\n	at org.apache.jasper.compiler.Node$Nodes.visit(Node.java:2302)\r\n	at org.apache.jasper.compiler.Node$Visitor.visitBody(Node.java:2352)\r\n	at org.apache.jasper.compiler.Node$Visitor.visit(Node.java:2358)\r\n	at org.apache.jasper.compiler.Node$Root.accept(Node.java:498)\r\n	at org.apache.jasper.compiler.Node$Nodes.visit(Node.java:2302)\r\n	at org.apache.jasper.compiler.Validator.validate(Validator.java:1878)\r\n	at org.apache.jasper.compiler.Compiler.generateJava(Compiler.java:215)\r\n	at org.apache.jasper.compiler.Compiler.compile(Compiler.java:431)\r\n	at org.apache.jasper.JspCompilationContext.compile(JspCompilationContext.java:608)\r\n	at org.apache.jasper.servlet.JspServletWrapper.service(JspServletWrapper.java:374)\r\n	at org.apache.jasper.servlet.JspServlet.serviceJspFile(JspServlet.java:476)\r\n	at org.apache.jasper.servlet.JspServlet.service(JspServlet.java:366)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:445)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:574)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:293)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:120)\r\n	at org.springframework.web.servlet.view.InternalResourceView.renderMergedOutputModel(InternalResourceView.java:209)\r\n	at org.springframework.web.servlet.view.AbstractView.render(AbstractView.java:267)\r\n	at org.springframework.web.servlet.DispatcherServlet.render(DispatcherServlet.java:1221)\r\n	at org.springframework.web.servlet.DispatcherServlet.processDispatchResult(DispatcherServlet.java:1005)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:952)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:870)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:961)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:852)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:837)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1317)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.obtainContent(SiteMeshFilter.java:129)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.doFilter(SiteMeshFilter.java:77)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:383)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:344)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:261)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:88)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:443)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:556)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:255)\r\n	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:154)\r\n	at org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:116)\r\n	at org.eclipse.jetty.server.Server.handle(Server.java:369)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.handleRequest(AbstractHttpConnection.java:486)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.headerComplete(AbstractHttpConnection.java:933)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection$RequestHandler.headerComplete(AbstractHttpConnection.java:995)\r\n	at org.eclipse.jetty.http.HttpParser.parseNext(HttpParser.java:644)\r\n	at org.eclipse.jetty.http.HttpParser.parseAvailable(HttpParser.java:235)\r\n	at org.eclipse.jetty.server.AsyncHttpConnection.handle(AsyncHttpConnection.java:82)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint.handle(SelectChannelEndPoint.java:667)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint$1.run(SelectChannelEndPoint.java:52)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:608)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:543)\r\n	at java.lang.Thread.run(Thread.java:744)\r\n');
INSERT INTO `sys_log` VALUES ('71f8c7166ac043598d426fd1a2c9c80f', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:31:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('72434e70d9014f62a45ae9b58940553d', '1', 'echarts-bar-bar1演示', '1', '2015-05-03 21:42:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('72677a82fa104c7d9a7ccbeaca300fc0', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:09:59', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('72bec5e5a7db425aa8ec986b16683dbe', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 14:25:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('731d50b511474759b63e0f18f5064688', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 14:33:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('733628b1f68b4fbfa4cba4bccac5df6a', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:36:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('738b73e734db4a0ead41842ae6925ec6', '1', '系统设置-系统设置-菜单管理', '1', '2015-04-18 19:04:13', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_20', '');
INSERT INTO `sys_log` VALUES ('7449b646cc934089ab20a70e5bdbad2d', '1', 'echarts-bar-bar4演示', '1', '2015-05-03 21:47:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar4', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('747773c2939e4c0e9aa7aad8a9ceb42c', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 13:06:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('74877d0ce84b45158aee6cc69f3716ab', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:30:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=210&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar6演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar6&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('74eb5e863d90468894cdeeececfbac6f', '1', '系统设置-机构用户-区域管理', '1', '2015-05-02 14:18:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/area/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('75c4c2527c7f4ea493059f6988f44da0', '1', 'echarts-bar-bar6演示', '1', '2015-05-03 21:42:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar6', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('75df17c813b84d0b9383ef390bd42d42', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:15:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('760ed797f46a45ecbb8fc76fceedfac9', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:06:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('7616637d8fe9483fb8d1b80ce8d0b2f4', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 13:53:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('76a28aff4b8f4fabbc20e9b29b10f305', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:10:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('7730e2689fa44bbd8dc95d8c6bf788c6', '1', 'echarts-bar-bar8演示', '1', '2015-05-03 21:48:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar8', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('77ccaa6a97d044d3830f5af69af31923', '1', 'echarts-bar-bar3演示', '1', '2015-05-03 21:35:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar3', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('7811767e473b41b1a9ffb76bd3086077', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-04-19 23:34:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/delete', 'GET', 'id=31', '');
INSERT INTO `sys_log` VALUES ('7885e4e0fef543eab3c88dbad3f2b86e', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 20:45:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('78e1b970b3714fddbd206535f5b637b0', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:49:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/delete', 'GET', 'id=23', '');
INSERT INTO `sys_log` VALUES ('78fceecdfb8748a39845b5b293c3e47c', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:31:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=60&parent.id=06fc54514cdf45d992c1f5403215d016&name=bar11演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar11&parent.name=echarts&isShow=1', '');
INSERT INTO `sys_log` VALUES ('79471066a28444c99877ef7a483d1e23', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:14:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('795832d208f14e848fa84606903e30ae', '1', 'echarts-bar-bar10演示', '1', '2015-05-03 21:34:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar10', 'GET', 'tabPageId=jerichotabiframe_28', '');
INSERT INTO `sys_log` VALUES ('7a06caf304854d07abb45f9ee015feec', '1', 'echarts-bar-bar7演示', '1', '2015-05-03 21:42:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar7', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('7a0df7c312e0412d957a5b52e1b5f298', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:35:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('7a3531a2e6e24ab6ba345bdcc4794847', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:48:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('7abd65c7f38c48678bbc6439c73d3968', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:48:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('7b2bc9aa7df245c0962fcb6c0c974ffb', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:30:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=300&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar9演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar9&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('7b6836bddbe14ded8655f648acc1a290', '1', 'echarts-bar-bar15演示', '1', '2015-05-03 21:34:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar15', 'GET', 'tabPageId=jerichotabiframe_32', '');
INSERT INTO `sys_log` VALUES ('7b6e7e3f6ecf4214a3ed4f99bfd1c2b1', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:09:24', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('7b838b4c4d3a436bba842763ddd49e1a', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:35:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('7b97c1a3037d4a478e944c3554d906ab', '1', '代码生成-生成示例-主子表', '1', '2015-05-02 13:07:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testDataMain', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('7c36a4ba6e0e41af906403b804b49a1f', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:29:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('7c4c9b863e6642e5ba87735eab03c7ed', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:24:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('7c94757d62e347409cca6feaf0a1513c', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:11:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('7ccc47facef3452b987af051d2187282', '1', '在线办公-通知通告-通告管理', '1', '2015-05-02 13:06:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('7ce8ab268bc04f238a8bbb4d4ab4c049', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 14:23:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('7d20c8737142482e9bcfa79ef0e6a4c4', '1', '在线办公-通知通告-通告管理', '1', '2015-04-21 22:13:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('7d3daca1e2aa4ba6bafc25e174b38a35', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:28:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('7d585263788b42a9a8d9b802379d0d2d', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 13:46:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('7ea788f2f80546339ffed8f96ff7b6f9', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:31:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/delete', 'GET', 'id=8f26e9cbba204846bedf0cab85e7dbe1', '');
INSERT INTO `sys_log` VALUES ('7ed4458b05a54e4a8c5a13fdca6375fb', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:31:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=330&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar10演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar10&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('7f2deac19d5b4bf3b7808bb906d886ac', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:39:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('7f6b8fdc41ab47258fcfcfa8f3847830', '2', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:34:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', 'java.lang.IllegalStateException: Neither BindingResult nor plain target object for bean name \'echartsdemo\' available as request attribute\r\n	at org.springframework.web.servlet.support.BindStatus.<init>(BindStatus.java:144)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getBindStatus(AbstractDataBoundFormElementTag.java:168)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getPropertyPath(AbstractDataBoundFormElementTag.java:188)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getName(AbstractDataBoundFormElementTag.java:154)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.autogenerateId(AbstractDataBoundFormElementTag.java:141)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.resolveId(AbstractDataBoundFormElementTag.java:132)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.writeDefaultAttributes(AbstractDataBoundFormElementTag.java:116)\r\n	at org.springframework.web.servlet.tags.form.AbstractHtmlElementTag.writeDefaultAttributes(AbstractHtmlElementTag.java:422)\r\n	at org.springframework.web.servlet.tags.form.InputTag.writeTagContent(InputTag.java:142)\r\n	at org.springframework.web.servlet.tags.form.AbstractFormTag.doStartTagInternal(AbstractFormTag.java:84)\r\n	at org.springframework.web.servlet.tags.RequestContextAwareTag.doStartTag(RequestContextAwareTag.java:80)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspx_meth_form_input_0(axisList_jsp.java:298)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspx_meth_form_form_0(axisList_jsp.java:257)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspService(axisList_jsp.java:121)\r\n	at org.apache.jasper.runtime.HttpJspBase.service(HttpJspBase.java:109)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.apache.jasper.servlet.JspServletWrapper.service(JspServletWrapper.java:403)\r\n	at org.apache.jasper.servlet.JspServlet.serviceJspFile(JspServlet.java:476)\r\n	at org.apache.jasper.servlet.JspServlet.service(JspServlet.java:366)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:445)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:574)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:293)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:120)\r\n	at org.springframework.web.servlet.view.InternalResourceView.renderMergedOutputModel(InternalResourceView.java:209)\r\n	at org.springframework.web.servlet.view.AbstractView.render(AbstractView.java:267)\r\n	at org.springframework.web.servlet.DispatcherServlet.render(DispatcherServlet.java:1221)\r\n	at org.springframework.web.servlet.DispatcherServlet.processDispatchResult(DispatcherServlet.java:1005)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:952)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:870)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:961)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:852)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:837)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1317)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.obtainContent(SiteMeshFilter.java:129)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.doFilter(SiteMeshFilter.java:77)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:383)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:344)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:261)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:88)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:443)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:556)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:255)\r\n	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:154)\r\n	at org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:116)\r\n	at org.eclipse.jetty.server.Server.handle(Server.java:369)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.handleRequest(AbstractHttpConnection.java:486)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.headerComplete(AbstractHttpConnection.java:933)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection$RequestHandler.headerComplete(AbstractHttpConnection.java:995)\r\n	at org.eclipse.jetty.http.HttpParser.parseNext(HttpParser.java:644)\r\n	at org.eclipse.jetty.http.HttpParser.parseAvailable(HttpParser.java:235)\r\n	at org.eclipse.jetty.server.AsyncHttpConnection.handle(AsyncHttpConnection.java:82)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint.handle(SelectChannelEndPoint.java:667)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint$1.run(SelectChannelEndPoint.java:52)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:608)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:543)\r\n	at java.lang.Thread.run(Thread.java:744)\r\n');
INSERT INTO `sys_log` VALUES ('7f8e6a4cad3e43958c9ce7b8b3941aa1', '1', '系统设置-系统设置-字典管理', '1', '2015-04-21 22:14:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/dict/', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('7fe04929ad8f4e8ebbc8a1ebe67fc5b8', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 14:19:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('806fffad2ce843478bac9e58b66d266d', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:06:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/delete', 'GET', 'id=62', '');
INSERT INTO `sys_log` VALUES ('80c86217dfe44984af3ba87b7df958df', '1', 'echarts-bar-bar8演示', '1', '2015-05-03 21:34:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar8', 'GET', 'tabPageId=jerichotabiframe_26', '');
INSERT INTO `sys_log` VALUES ('8123d04af5484d9793ee48856601f42f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:32:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('81599e768be24aa997a260811d3f8dca', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:36:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('819c20d826634b6ca5a733b547090080', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:49:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('823b86bc3c144edbb70e65f7ad32bd0e', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:32:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=60&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar12演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar12&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('82410290a6d845f6980b5336f31e9f73', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-21 21:46:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('827b7075dba44625b6f2159849d2c169', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:04:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('82acf44bc4594130b1ec9d88df44ce24', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 20:45:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=40&parent.id=24ebfed0acae410a86c81628d2659a67&name=line演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('833b162e05164bdfa8908bac06427916', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('8341a2c6ff1442349c8a187f521dcfc2', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:36:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('8392218c1d474b7ba9245a831ba1ff3c', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:28:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('840239efd38a4d009afe9bcea2afe881', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:13:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('842f26509e134f36b71640075aa6f3bc', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:13:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('8485596fb4894a8a98d82bf0ae0e4b79', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:47:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('8517d07f47b44fc5bc8ad9d2b8d00df3', '1', '在线办公-个人办公-我的任务', '1', '2015-04-21 22:13:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/task/todo/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('853509680d144758a2773e40da149ca1', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:33:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('8539548f123d48ada286f02681b361d3', '1', '在线办公-流程管理-模型管理', '1', '2015-05-02 13:06:13', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/model', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('853c390e1b1f4c1ea4f59305767a6f2b', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:28:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('8590cd7c32f449f3bae3291ea8babf93', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 15:06:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('86392ad77ade43d4918de8074321f86d', '1', '系统设置-系统设置-角色管理', '1', '2015-04-18 19:04:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_21', '');
INSERT INTO `sys_log` VALUES ('880a86d2b01c49b1a322190884114da2', '1', '系统登录', '1', '2015-05-02 20:44:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('881af69d25fb43e588bc5f8bcf1c0965', '1', '系统登录', '1', '2015-05-02 15:05:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('889a1572ec114c99b99f138b623bb747', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 13:51:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=10&parent.id=e3234be734134047bc8770571d5d2c0d&name=爬虫监控&target=&permission=&remarks=&href=&parent.name=监控&isShow=1', '');
INSERT INTO `sys_log` VALUES ('89890b9f82c84c269c4abcd4d46e5ddc', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 20:46:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('89caf8bb074a42da8c777fdff64231bf', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:45:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('8a40286b553e4762b2acc65e4bf96f0f', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:59:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('8a584169117344158383a20f95348c77', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 21:02:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('8ad2ec7c113e4ad28b413a99ffe6eb11', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:13:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=200&parent.id=24ebfed0acae410a86c81628d2659a67&name=line8演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line8&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('8b4f8c346628438399d2cdb04e3228ca', '1', '在线办公-通知通告-我的通告', '1', '2015-05-02 13:06:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify/self', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('8bb7585972254e14b115f800381b9a05', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:36:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'GET', 'tabPageId=jerichotabiframe_20', '');
INSERT INTO `sys_log` VALUES ('8bc5344994ac4b5d96fe7c42d9898a3b', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:22:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('8be35136c9264b9985aa0f63675f20d5', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:56:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('8be414bc3028450cb59c4baf42d336bc', '1', '系统设置-日志查询-日志查询', '1', '2015-04-21 22:14:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/log', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('8c3b17df67f34bf7b5cc1bf1d17a2b62', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:57:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('8c46fc78e9714ba19b252d9a794ec4ad', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:04:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=1948167a2d504222b470899fbd139ec9&icon=&sort=30&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('8c78762e7ef344219c062dbdb2cc6821', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:05:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('8c8ed77062d64d5dad65883dff941f36', '1', '系统登录', '1', '2015-05-02 13:52:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('8c902970db344d4fa3039a3789f841ba', '1', '在线办公-通知通告-我的通告', '1', '2015-04-21 22:13:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify/self', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('8cbd8814417948dfa716b404b26279d0', '1', '在线办公-流程管理-模型管理', '1', '2015-04-18 19:02:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/model', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('8cca364503a3443bb6034ef7904e6966', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:34:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'GET', 'tabPageId=jerichotabiframe_16', '');
INSERT INTO `sys_log` VALUES ('8d00dd4b626e4f9cbabc00769ce7c3f0', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:11:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=70&parent.id=24ebfed0acae410a86c81628d2659a67&name=line3演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line3&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('8d983f512ef04f80b3c5757a3c043ac1', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:14:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('8dde6d8030fb48c3ac0bc3f732476e48', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 13:48:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('8e1932b605ac4826997ff3c8142b8603', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:36:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('8e3b219d63ce448395d0a59a9705e0c6', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:42:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('8e50c836d10b414c84fc76873b327938', '1', '系统登录', '1', '2015-05-02 21:40:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('8ec5cc995a75454297f64c63376e8d52', '1', '系统登录', '1', '2015-05-03 21:42:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('8f7cf98632c947c88f84454e94ba7422', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:32:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=420&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar14演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar14&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('90007c60a4bb4be89846b36ff220f181', '1', '在线办公-通知通告-我的通告', '1', '2015-04-21 21:47:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/oaNotify/self', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('90f99dcc9e6c425cb4f6933f0d3a885e', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:30:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('911510d2a9a243078e6192057dcc76c4', '1', '代码生成-生成示例-树结构-查看', '1', '2015-05-02 13:07:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('91232ae0eccc4eeb85889ce557e5ce62', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('91a39421f27345a6a9250b244db3e941', '1', 'echarts-bar-bar8演示', '1', '2015-05-03 21:35:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar8', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('92635363dc084eaf9ab7f6e66d9f6f67', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:32:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('93490bb352dc42c8bd453970003bd376', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:41:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('9349ba2778654292bb328971b28a3fe1', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:15:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('93ed5a9d8af54ec99f4e5b842462ad81', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:49:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('940220ae9d1c43f3b9968e98558f59c3', '1', '代码生成-代码生成-业务表配置', '1', '2015-04-19 23:34:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('95853331e21a406cb2d0b1d7fa89c7ac', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:49:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('95c259debb764f0080b601abb88678d4', '1', 'echarts-bar-bar8演示', '1', '2015-05-03 21:34:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar8', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('95f89b1e1da34bacaac99f56fc73f58c', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:15:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'GET', 'tabPageId=jerichotabiframe_16', '');
INSERT INTO `sys_log` VALUES ('9677026c859e442fae2d577fd31a3490', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:45:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('9677514e29c14b9fad8d61187005e66f', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 14:19:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('96bd166bc71b4aeeb708b539babccaff', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-03 21:33:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('96dee99459cf47fb87b90c75b93c9a8a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 20:56:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('970b28d8639b4de3b7a6e2429ae522ca', '1', 'echarts-bar-bar10演示', '1', '2015-05-03 21:36:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar10', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('9919793f1e2d4a61b733a5034a412f9a', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 20:46:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('99c769ebb1a84ceb999e8b742924f169', '1', '系统登录', '1', '2015-05-02 14:47:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('9a04dc5190f7400e88ebb758ee84b5f8', '1', '在线办公-流程管理-模型管理', '1', '2015-04-21 22:13:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/model', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('9a2bfbd7439e48aebf1b330c88667cfb', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:09:58', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('9b66d5c0227042dd8d4cd823fa312009', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:03:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('9cbabb73191a4a0fbaa442103171f445', '1', 'echarts-line-line演示', '1', '2015-05-02 21:15:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('9cc6a195420a40b8b453e035e80c0f07', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 14:34:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('9cf62ac5078b4e9dac3d3f4314210d5d', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 20:58:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('9cfb113797c6470f9a4d45514319dd9c', '1', 'echarts-bar-bar14演示', '1', '2015-05-03 21:42:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar14', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('9d5d2742bbb040c08c212fd781454801', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 15:06:51', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('9d9cc7f4def44306add177b6fd27f64f', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('9dca8d87742142d99d372a748a726e32', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:29:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('9df5e867fd664a64aa1d4a4d2859e66d', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:04:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('9e44f94b203640f48f2edfd56c6563cc', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:12:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=80&parent.id=24ebfed0acae410a86c81628d2659a67&name=line4演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line4&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('9eb15c68c3a046c4857780e8bec86840', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:18:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('9f14310bebaf499c97bac353b3f2bc80', '1', '系统登录', '1', '2015-05-03 21:46:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('9f4c6825bda14eeb93ed1c0da6195153', '1', 'echarts-bar-bar10演示', '1', '2015-05-03 21:48:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar10', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('9ff0e5fce6cf481095e9b7bd08d544f2', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:38:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('9ff761488e4a46548e69f844cb2d8731', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 21:29:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('a0024c0c238a4e04b7df52b22a0ea0ad', '1', 'echarts-line-line演示', '1', '2015-05-02 21:28:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('a055aaf3e38f4916901eccab364dbb96', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:33:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a1642c80e4b9411eafd590757cf5b94b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:40:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a17ba526a25d4f3aaa068d43c80447dc', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:36:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('a1ae7e5a5b7c42478c5a7c09b23e7ad9', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:33:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('a1b70d4584974f1e810080b27ac4b708', '1', '系统设置-系统设置-菜单管理', '1', '2015-04-17 23:36:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('a1debe414d274b929de481aa16b9c038', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:56:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('a208e91c6e634cd39665a3a95ab5412d', '1', 'echarts-bar-bar3演示', '1', '2015-05-03 21:33:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar3', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('a31a2e395af34d6589406dda0aa7e623', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:58:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('a34b4af1201e4f7a9813b1fe115ea8f0', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:52:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a371852c705a442f9c3062903dcc4479', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:29:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('a398e7864966497fb074de1233052419', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:04:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a47b368763f546e5af609d3633f77f69', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:02:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=80&parent.id=24ebfed0acae410a86c81628d2659a67&name=line&target=&permission=&remarks=&href=&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('a4a013f51fad425498c8c0b5ecb3ea9b', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:50:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=e3234be734134047bc8770571d5d2c0d', '');
INSERT INTO `sys_log` VALUES ('a4d9e052e6a746f89fc7ae205ba12ef7', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 15:06:49', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('a4ed5e6c417d41b1b744a39ece7aea01', '1', 'echarts-bar-bar10演示', '1', '2015-05-03 21:42:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar10', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('a53d93b72b7f44d29a07afd53ea6e3db', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:35:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('a548dd26e3b34be5bf283391d45712e4', '1', '内容管理-内容管理', '1', '2015-04-18 19:03:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/none', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('a65fb9b100904a58b33645b210925034', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:46:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a667645c58ab43f581e3911b1c340593', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 21:02:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('a6babe82fe39402a8798c4eff507c917', '1', '代码生成-生成示例-树结构', '1', '2015-05-02 13:07:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('a6bc510e9cf94d619bb11d8c0e58cd16', '1', '代码生成-生成示例-树结构-查看', '1', '2015-05-02 13:07:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('a726ee390a7343e4a49a4663a1314c52', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 14:18:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('a75d03c52b304deca0f3505be0d030bd', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:36:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('a81a5abedfa74f4ebb16d1e8bfd5613b', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 13:52:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('a882a34397cd48978ca6ac411ded0d8e', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:32:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('a8c7648899ab4aeca13f3f8cf6d5bb02', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 13:06:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('a8cda4180167420997f47062f1ef471b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:32:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('a8ceda620a5f4ddb88704fef9f97317c', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:02:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('aa0eb2101cb141a89d0a4dfb5b1a17cc', '1', 'echarts-bar-bar4演示', '1', '2015-05-03 21:42:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar4', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('aa71ce46bf4443238edc34c0853b1852', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:54:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('aae36fba9f944c14b1dbf062a27c2495', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:36:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('aae76618c42d4d2cbe2abef63392aa48', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:32:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('ab1c4e6a9a9f4e859884f58cadd000cf', '1', '系统登录', '1', '2015-05-02 21:10:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('ab6313784da5420aa9377ee471813b9a', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:36:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('ab9c63f02b074e66909dfc6203d2b25e', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:46:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('abdc746233b3471fab710c8c2f3b6ca4', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 15:06:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=1948167a2d504222b470899fbd139ec9', '');
INSERT INTO `sys_log` VALUES ('abff767f75764934b45b4edb7aeeda02', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:42:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('ac40da4f85724af29d9b6b83face4f0c', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:28:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('ac58a6686f7942a5b959e5be1f710686', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:28:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('ac6e0ff026e2467391d74600573c4f61', '1', '系统设置-机构用户-用户管理', '1', '2015-04-21 21:46:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('ad22e763d48844729f60d6696d363a03', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:13:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=140&parent.id=24ebfed0acae410a86c81628d2659a67&name=line6演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line6&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('ad418630d51d4e3391c23d25f0df999e', '1', 'echarts-bar-bar5演示', '1', '2015-05-03 21:42:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar5', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('ad42d53a7e944d83a14e315bf9ff65bc', '1', '代码生成-生成示例-主子表', '1', '2015-04-21 22:14:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testDataMain', 'GET', 'tabPageId=jerichotabiframe_20', '');
INSERT INTO `sys_log` VALUES ('ad909d8926634915954d0a0d57ce9cad', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:29:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('ad9320ab5b9943c68cd66e2014bfb8ed', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 15:07:03', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('ad9e800110a6452ba80ff541847d9583', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:45:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('adbaf96a4d934325909ce1fec7347ff9', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:15:32', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('ade4037de9714b56801f1ecd20748bbf', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 13:52:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('ae257af42a324802a8f1a7f93acd9a10', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:59:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('ae50399452db4a4f9736d95f7e107867', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:33:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('ae58725c14af41a982a30a0b2e6317f2', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:39:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('aecf8d6393004e3686f9959021c08b76', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 20:56:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('aef0e488bb374aa2833848c033ff52de', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:45:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('b0015e2f98f040daa3764e0109cb1e33', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:15:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('b0246e4a6bfe4702ab8fb50205d2c6f2', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:59:56', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('b02cdc12f7b04968bdf4bdcf2e914613', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:40:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('b15a8a5ef0ef4ff9b9fcfac332b2bd68', '1', '系统登录', '1', '2015-05-02 15:07:19', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('b1ac3aa090c24296ada3f445e68525c6', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:33:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('b2adf1c634614a34ad67f4364e68c33a', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:55:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('b2ff306532e84498b6cf251029b3412a', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('b32502f122d44c72b3084d9f4cdade1d', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:27:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('b3cf2cc4df4749948918977dc3a02062', '1', '内容管理-内容管理-公共留言-查看', '1', '2015-04-18 19:03:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/guestbook/', 'GET', 'status=2&tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('b44d4849cd364f3a9d7af5039e88570e', '1', '代码生成-生成示例-树结构-查看', '1', '2015-05-02 13:07:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('b45a493411fc4f3e9b62ceb75dde2435', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 20:46:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/assign', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('b4c3c2af019d4932aee7419975073a11', '1', '系统登录', '1', '2015-05-02 14:54:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('b50842df80d4445a9fa176bcdc5973a2', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 21:01:29', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('b5ea5c8d421f4b7fa5b8d48ee2b499c2', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:15:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('b6a5dc64dd2e4555965d45e4983bc518', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:15:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('b73bcebc6f794f53b151eed8e19ef838', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:42:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('b75f8b4905134208a9ec264fb241f7fa', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:39:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('b77da536ee77430395552c5729c1ded6', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 20:45:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=1948167a2d504222b470899fbd139ec9&icon=&sort=30&parent.id=24ebfed0acae410a86c81628d2659a67&name=bar演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('b8d28e410f654041baa06537880bb389', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:14:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=290&parent.id=24ebfed0acae410a86c81628d2659a67&name=line11演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line11&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('b8d376a66a2f4f27af5221f2b5fbb641', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 21:29:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('b8df0b38cbdd4efa86e5c16b77e22111', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:06:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('b915f47e7082414f9ed61d08bfa21dcb', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:10:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('b93c31f7150b4e25927ddd7e6db5ac5d', '1', 'echarts-line-line演示', '1', '2015-05-02 21:10:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('b94b9e7ab7fd45c3909690ffe2a61cc6', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 14:33:35', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('b968cde450b244b2a352ad7a66f934c3', '1', '系统登录', '1', '2015-05-02 13:05:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('b96ab26a4d93411bbbe09f8980358f32', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:26:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('ba56893d06104cdab1a43f0ff75a2116', '1', '系统设置-系统设置-菜单管理', '1', '2015-04-21 22:13:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('baa6e48134ce4d4a82893a60128ef61c', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-05-02 14:18:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/list', 'GET', 'id=&parentIds=', '');
INSERT INTO `sys_log` VALUES ('bb5bd97e428d4a689f8d48cccc466fcf', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 15:09:37', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('bbdc49959f4a4a1db4f5360d5c507b09', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:15:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('bc08610a07014c9b8cf6fb00fa24e0ec', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 21:10:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('bc171cca81374e83a2432b5a58a5a8b1', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:32:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('bc4f0293d3e34fab9110fa21cea76ff7', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 14:23:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('bca47c8b7e8744e7934f8c43aa9e00ae', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('bcd208c6e69641fda4836090de261dc1', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 13:52:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('bce8308d3d3e4bf480848e77eeeaa620', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 14:23:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('bd2e4987910846e58351e516e25f654f', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:30:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('bd82ea15dfd749a39bfcf2b7a5112f1e', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:34:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'GET', 'tabPageId=jerichotabiframe_30', '');
INSERT INTO `sys_log` VALUES ('be38eff9de8146dc8140a4a48833fe29', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 15:39:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('be5c7af4981e4172b66dc8499671c001', '1', '我的面板-个人信息-修改密码', '1', '2015-05-02 13:06:02', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('be8ae64b43a44b9998ec33704ac27292', '1', '代码生成-生成示例-树结构-查看', '1', '2015-05-02 13:07:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testTree/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('bfa9dc0d81db416ea045606241ec792e', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-04-21 21:46:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/list', 'GET', 'id=&parentIds=', '');
INSERT INTO `sys_log` VALUES ('bfbd626a360143fb9f4f52a3a9071b57', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-02 13:52:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=2', '');
INSERT INTO `sys_log` VALUES ('c07bdbfa6e8e4947bdb4403ba91f3050', '1', 'echarts-line-line1演示', '1', '2015-05-02 21:15:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('c07f03b9b91949868b9a67e54ebef82a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 14:24:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=06fc54514cdf45d992c1f5403215d016', '');
INSERT INTO `sys_log` VALUES ('c1255ccde73b453181b64e6b67d38f7b', '1', '内容管理-内容管理-内容发布', '1', '2015-04-19 23:34:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('c17ccac93bbe47d3b48dad0c903899f5', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:30:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('c20153c33ce74f178711eedb0d02d918', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:43:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('c22a10f8549c4679ba63a473b2aecbad', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:15:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('c232961b624a4b9faca1c25d71e0e12b', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:31:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('c24d9eaed4e74bcb9bcf9c2ac37fef17', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:12:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('c2d40b0466424d93ab9ac2c714ad2a4e', '1', '代码生成-代码生成-业务表配置', '1', '2015-05-02 13:06:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('c2ee0b648d9e4617b48d15e4558180d9', '1', 'echarts-line-line6演示', '1', '2015-05-02 21:19:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line6', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('c30e96b2c1d543478a0efc507047d500', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:36:32', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('c4a36f64a6854ee5843f3f098f9561b8', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:47:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('c50f32f82ccf4542986ce6f21c1ab521', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:36:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('c55c8b29235d48998056695a233d15bc', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 21:10:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('c5b7af8a953b4ab2bf89ebfa1c24d5d3', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-04-21 22:13:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/form', 'GET', 'parent.id=', '');
INSERT INTO `sys_log` VALUES ('c6b9d96070b04f508708d3f552263dcb', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:15:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('c6dcbf43bc7044499ffc7369496d8fe8', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:14:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('c8128273449a41db82c730216f9e5358', '1', '系统设置-机构用户-区域管理', '1', '2015-04-18 19:04:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/area/', 'GET', 'tabPageId=jerichotabiframe_19', '');
INSERT INTO `sys_log` VALUES ('c878448592124e7cbb59113cdcaaab11', '1', '系统登录', '1', '2015-05-02 14:55:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('c889c068e28e47b892425a8f0bb3371c', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:28:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('c88edc4d05cd415e995d33709c69673b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:42:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('c90f37da9dd049788b540583451f641c', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:31:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=06fc54514cdf45d992c1f5403215d016', '');
INSERT INTO `sys_log` VALUES ('c945a4f2ad424292bd2f2f3098f283cb', '1', '系统登录', '1', '2015-05-02 21:04:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('ca13bcc79b324c2bac6a8ac6c10d58d5', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:14:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('ca1475cf9ec44a9685e88f4bc3824bb0', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:32:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=450&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar15演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar15&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('ca35bf8e9ac646f2babad548b0808da2', '1', '系统登录', '1', '2015-05-02 21:27:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('ca56fe7e33394510951c1a639fd92035', '1', '在线办公-流程管理-流程管理', '1', '2015-04-18 19:02:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/process', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('cc013c3774214ef2acc019cff3edfe73', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:38:43', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('cd675b23419d423d9a7da319c3e2524b', '1', 'echarts-line-line7演示', '1', '2015-05-02 21:36:26', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line7', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('cdd10bfdd0a14eca9415be66a2a689bc', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:04:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('ce2882f2999a45919d2e593992de10ba', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:45:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('cea0613f0569428e993f12ecefbe9ac9', '1', 'echarts-line-line演示', '1', '2015-05-02 21:04:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('cee2ca0af52b4e1bbf7946f3b6cefec7', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:30:23', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('cefeca5855e842f6a3ce87fd5b75c9fc', '1', '我的面板-个人信息-个人信息', '1', '2015-05-03 21:35:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('cf1e10a771e74d2bbed960cb8dcb53bc', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:33:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('cf1f80ddaf964c62a615a9a2200808e1', '1', 'echarts-bar-bar11演示', '1', '2015-05-03 21:48:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar11', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('d00b6cd4a67b465c88ee48c03aa0a287', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 14:33:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('d04a75c5bfdc4728ae490b608a0f8cc4', '1', 'echarts-line-line3演示', '1', '2015-05-02 21:16:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line3', 'GET', 'tabPageId=jerichotabiframe_18', '');
INSERT INTO `sys_log` VALUES ('d064fe51219547ad9c0ca8872b36def5', '1', '在线办公-个人办公-审批测试', '1', '2015-04-21 22:13:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/oa/testAudit', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('d082ab90f277428186eb2b7e8a76a652', '1', '内容管理-内容管理-内容发布', '1', '2015-04-18 19:03:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('d0fce031ee9a476a8ca112d5b73969f7', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 13:06:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('d1078752883245bcb6b46dae6fd3b200', '1', 'echarts-bar-bar7演示', '1', '2015-05-03 21:34:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar7', 'GET', 'tabPageId=jerichotabiframe_25', '');
INSERT INTO `sys_log` VALUES ('d11a57ae94944914ae46a65e9bcc95fd', '1', '我的面板-个人信息-个人信息', '1', '2015-04-21 22:13:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('d1cae30b7f654d21967be03b2dd05567', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:43:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('d21a8bdf68b249c3b8c100a4abe4d8e8', '2', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:35:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_3', 'java.lang.IllegalStateException: Neither BindingResult nor plain target object for bean name \'echartsdemo\' available as request attribute\r\n	at org.springframework.web.servlet.support.BindStatus.<init>(BindStatus.java:144)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getBindStatus(AbstractDataBoundFormElementTag.java:168)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getPropertyPath(AbstractDataBoundFormElementTag.java:188)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.getName(AbstractDataBoundFormElementTag.java:154)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.autogenerateId(AbstractDataBoundFormElementTag.java:141)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.resolveId(AbstractDataBoundFormElementTag.java:132)\r\n	at org.springframework.web.servlet.tags.form.AbstractDataBoundFormElementTag.writeDefaultAttributes(AbstractDataBoundFormElementTag.java:116)\r\n	at org.springframework.web.servlet.tags.form.AbstractHtmlElementTag.writeDefaultAttributes(AbstractHtmlElementTag.java:422)\r\n	at org.springframework.web.servlet.tags.form.InputTag.writeTagContent(InputTag.java:142)\r\n	at org.springframework.web.servlet.tags.form.AbstractFormTag.doStartTagInternal(AbstractFormTag.java:84)\r\n	at org.springframework.web.servlet.tags.RequestContextAwareTag.doStartTag(RequestContextAwareTag.java:80)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspx_meth_form_input_0(axisList_jsp.java:229)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspx_meth_form_form_0(axisList_jsp.java:188)\r\n	at org.apache.jsp.WEB_002dINF.views.modules.echartsdemo.axisList_jsp._jspService(axisList_jsp.java:110)\r\n	at org.apache.jasper.runtime.HttpJspBase.service(HttpJspBase.java:109)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.apache.jasper.servlet.JspServletWrapper.service(JspServletWrapper.java:403)\r\n	at org.apache.jasper.servlet.JspServlet.serviceJspFile(JspServlet.java:476)\r\n	at org.apache.jasper.servlet.JspServlet.service(JspServlet.java:366)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:445)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:574)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:293)\r\n	at org.eclipse.jetty.server.Dispatcher.forward(Dispatcher.java:120)\r\n	at org.springframework.web.servlet.view.InternalResourceView.renderMergedOutputModel(InternalResourceView.java:209)\r\n	at org.springframework.web.servlet.view.AbstractView.render(AbstractView.java:267)\r\n	at org.springframework.web.servlet.DispatcherServlet.render(DispatcherServlet.java:1221)\r\n	at org.springframework.web.servlet.DispatcherServlet.processDispatchResult(DispatcherServlet.java:1005)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:952)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:870)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:961)\r\n	at org.springframework.web.servlet.FrameworkServlet.doGet(FrameworkServlet.java:852)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:707)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:837)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:820)\r\n	at org.eclipse.jetty.servlet.ServletHolder.handle(ServletHolder.java:652)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1317)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.obtainContent(SiteMeshFilter.java:129)\r\n	at com.opensymphony.sitemesh.webapp.SiteMeshFilter.doFilter(SiteMeshFilter.java:77)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter$1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:383)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:344)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:261)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:88)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.eclipse.jetty.servlet.ServletHandler$CachedChain.doFilter(ServletHandler.java:1288)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doHandle(ServletHandler.java:443)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:137)\r\n	at org.eclipse.jetty.security.SecurityHandler.handle(SecurityHandler.java:556)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doHandle(SessionHandler.java:227)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doHandle(ContextHandler.java:1044)\r\n	at org.eclipse.jetty.servlet.ServletHandler.doScope(ServletHandler.java:372)\r\n	at org.eclipse.jetty.server.session.SessionHandler.doScope(SessionHandler.java:189)\r\n	at org.eclipse.jetty.server.handler.ContextHandler.doScope(ContextHandler.java:978)\r\n	at org.eclipse.jetty.server.handler.ScopedHandler.handle(ScopedHandler.java:135)\r\n	at org.eclipse.jetty.server.handler.ContextHandlerCollection.handle(ContextHandlerCollection.java:255)\r\n	at org.eclipse.jetty.server.handler.HandlerCollection.handle(HandlerCollection.java:154)\r\n	at org.eclipse.jetty.server.handler.HandlerWrapper.handle(HandlerWrapper.java:116)\r\n	at org.eclipse.jetty.server.Server.handle(Server.java:369)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.handleRequest(AbstractHttpConnection.java:486)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection.headerComplete(AbstractHttpConnection.java:933)\r\n	at org.eclipse.jetty.server.AbstractHttpConnection$RequestHandler.headerComplete(AbstractHttpConnection.java:995)\r\n	at org.eclipse.jetty.http.HttpParser.parseNext(HttpParser.java:644)\r\n	at org.eclipse.jetty.http.HttpParser.parseAvailable(HttpParser.java:235)\r\n	at org.eclipse.jetty.server.AsyncHttpConnection.handle(AsyncHttpConnection.java:82)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint.handle(SelectChannelEndPoint.java:667)\r\n	at org.eclipse.jetty.io.nio.SelectChannelEndPoint$1.run(SelectChannelEndPoint.java:52)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool.runJob(QueuedThreadPool.java:608)\r\n	at org.eclipse.jetty.util.thread.QueuedThreadPool$3.run(QueuedThreadPool.java:543)\r\n	at java.lang.Thread.run(Thread.java:744)\r\n');
INSERT INTO `sys_log` VALUES ('d2489dff5496423d9a568c3983680742', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:57:20', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('d26ba8e637434c1fa612b89d9c07bcf7', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:29:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=120&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar3演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar3&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('d271d0504d6749619173d8806b5a5f23', '1', 'echarts-bar-bar15演示', '1', '2015-05-03 21:34:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar15', 'GET', 'tabPageId=jerichotabiframe_18', '');
INSERT INTO `sys_log` VALUES ('d40f640bbcf74c57afa02244539311db', '1', '代码生成-生成示例-单表', '1', '2015-05-02 13:07:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testData', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('d4d77a89b4474de1af5d97883867fa70', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:45:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('d5f953395a54459f9658e3661dde4e48', '1', 'echarts-bar-bar15演示', '1', '2015-05-03 21:48:58', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar15', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('d64e21aa10a54c46a21be02e82fdc437', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:02:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('d67761a4ff104400af77226fbe3eda1d', '1', 'echarts-bar-bar11演示', '1', '2015-05-03 21:34:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar11', 'GET', 'tabPageId=jerichotabiframe_15', '');
INSERT INTO `sys_log` VALUES ('d6edf2438ef04a8d84626367734eb6a6', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-05-02 14:18:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('d73e5c210abc4a12a296319456734517', '1', '系统设置-机构用户-用户管理', '1', '2015-05-03 21:34:45', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_33', '');
INSERT INTO `sys_log` VALUES ('d7926013e7104bd98f6b64d95de75bba', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 13:46:07', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('d812f3a013b24f249b6e89ae223b4e4d', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:41:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('d8adb371bb17452e857cffdc0028746e', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:23:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('d8bdef8a59634b47ba3e5d43110b6c4f', '1', '系统登录', '1', '2015-05-02 13:06:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('d9529cc48a364835884e9ed3710d46d6', '1', '代码生成-生成示例-单表', '1', '2015-05-02 13:45:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testData', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('d991413586ea4e93a8c7e7c191666183', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:49:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('d9a7738c8b2b428da2658c54a66d7a07', '1', '系统设置-系统设置-角色管理-修改', '1', '2015-05-02 20:57:11', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/save', 'POST', 'dataScope=1&officeIds=&remarks=&office.id=2&oldName=系统管理员&menuIds=1,27,28,29,30,71,56,57,58,59,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,66...&id=1&useable=1&office.name=公司领导&name=系统管理员&roleType=assignment&sysData=1&enname=dept&oldEnname=dept', '');
INSERT INTO `sys_log` VALUES ('da16a459f7ce439e8241aeb5eaa37ace', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:29:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('da22b9039dac46e7a68da97deb3a2d54', '1', '系统登录', '1', '2015-05-02 20:56:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('da72dc1383ce40108dceecf6ebda046e', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 14:34:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=30&parent.id=24ebfed0acae410a86c81628d2659a67&name=axis演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/axis&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('dad67ed69ebb48f9b299b5dac84114ea', '1', '在线办公-流程管理-流程管理', '1', '2015-05-02 13:06:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/process', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('db1f5a9d7dcb4d1c95f770ac6f50d0e8', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:32:38', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('dbd0c37fc98943f89ddc509a2f9c039b', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:12:48', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('dc3273366b604e6791faf0029f68d76b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 15:05:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('dccec0e6a17344d8847ddefd94ec2865', '1', 'echarts-bar-bar14演示', '1', '2015-05-03 21:34:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar14', 'GET', 'tabPageId=jerichotabiframe_17', '');
INSERT INTO `sys_log` VALUES ('dcf988dabc6f48f5a7f0eecdb57c6b38', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:39:14', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('dd31380cbc124da8a8f1f983a61b8dab', '1', '系统设置-系统设置-菜单管理', '1', '2015-04-19 23:34:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('dd74d4da9d12463b986abf4125b2644a', '1', '在线办公-个人办公-我的任务', '1', '2015-05-02 13:06:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/act/task/todo/', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('de4361bc715d4179b969cc803351924b', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:45:40', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('de6d48b9f32b440c8955b8403bd50cc5', '1', 'echarts-bar-bar12演示', '1', '2015-05-03 21:33:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar12', 'GET', 'tabPageId=jerichotabiframe_5', '');
INSERT INTO `sys_log` VALUES ('dee702dc9a8b46b4825a0a5a4f1a671a', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:12:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=24ebfed0acae410a86c81628d2659a67', '');
INSERT INTO `sys_log` VALUES ('df92820958e944a9a35b23bef7066516', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 21:14:03', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=230&parent.id=24ebfed0acae410a86c81628d2659a67&name=line9演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line9&parent.name=line&isShow=1', '');
INSERT INTO `sys_log` VALUES ('e045f468247542989512ca9746eac9fc', '1', 'echarts-line-line2演示', '1', '2015-05-02 21:33:04', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line2', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('e048d3ec8ade43299b938f971259a500', '1', '内容管理-栏目设置-站点设置', '1', '2015-04-18 19:03:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/site/', 'GET', 'tabPageId=jerichotabiframe_14', '');
INSERT INTO `sys_log` VALUES ('e0d73c7f64db47a6ad81f9546df3e256', '1', 'echarts-bar-bar12演示', '1', '2015-05-03 21:42:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar12', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('e12758eeb6f24e06be8f5d0a5186d57c', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-04-18 19:04:10', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/list', 'GET', 'id=&parentIds=', '');
INSERT INTO `sys_log` VALUES ('e12843b780aa4f489ccd670ad15081bf', '1', '代码生成-生成示例-单表', '1', '2015-04-21 22:14:21', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/test/testData', 'GET', 'tabPageId=jerichotabiframe_19', '');
INSERT INTO `sys_log` VALUES ('e1329ea689a640a0ac850dbbe435551f', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:57:56', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e163bbf6534f4c0e866310aaec86ed1b', '1', 'echarts-bar-bar14演示', '1', '2015-05-03 21:37:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar14', 'GET', 'tabPageId=jerichotabiframe_21', '');
INSERT INTO `sys_log` VALUES ('e1a07c1908bf49c587e6e3ba07027339', '1', '系统登录', '1', '2015-05-02 21:14:59', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('e2fddc87291b4fc1989ddb86d0f3121e', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:13:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('e36929e8d2984df390b295e56a280325', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:47:15', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('e3c45fb9530843ee9ac1b65243e4a41e', '1', '代码生成-代码生成-业务表配置', '1', '2015-05-02 13:46:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genTable', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('e40f52a9f4ee46bfb0dc1edebf439ca7', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:36:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e41f3269f92443838f3dd9f95efa6653', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-02 20:56:54', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=50&parent.id=24ebfed0acae410a86c81628d2659a67&name=line1演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/line1&parent.name=demo演示&isShow=1', '');
INSERT INTO `sys_log` VALUES ('e4525ced4eb34124acc9ed72ae0e2daf', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 21:03:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('e46810c4747148d99792a887f3f9a944', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-19 23:34:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('e4b4c7d4349745be953f331dea9c0a3d', '1', '系统登录', '1', '2015-05-02 20:59:53', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('e4d5419ca8d6423ca3826ec8a12a5035', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 13:48:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e636d1112b7c481b8ff94d357d418044', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:25:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'GET', 'tabPageId=jerichotabiframe_8', '');
INSERT INTO `sys_log` VALUES ('e6aaea1c912c468abb91d263f8b8bfc6', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:18:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e6b6d09eeb3140e2bab8e767634460a7', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 20:57:57', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('e6d25d1a3a934009a4259a64ed0e2640', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 14:44:37', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/axis', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('e6fac4c96f864f4a94f2ab1737f2ad2b', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 15:16:25', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e7132f4ff7d64fbab641e00f20743de7', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:30:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=270&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar8演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar8&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('e71aa2ba674e416aa777f70b8dea3bc0', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:51:29', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('e83e736a9531457f8aa35b39a43ac377', '1', '代码生成-代码生成-生成方案配置', '1', '2015-05-02 13:43:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genScheme', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('e85f148c8f124e2ca59530201631e317', '1', '我的面板-个人信息-修改密码', '1', '2015-04-21 21:46:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('e8e301deb703485292fa7ae5ae417ff8', '1', 'echarts-line-line11演示', '1', '2015-05-02 21:15:12', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line11', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('e91dd2a1e958465b82c426df97bc5bd7', '1', '内容管理-内容管理-评论管理-查看', '1', '2015-04-18 19:03:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/cms/comment/', 'GET', 'status=2&tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('e9a54e0f760746f196db00f3de61039a', '1', 'echarts-bar-bar13演示', '1', '2015-05-03 21:45:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar13', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('ea256eb88c704c60b8314b89dd9ea0cf', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:30:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('ea9f34ecdcad405b87bf8e3ffc912074', '1', '系统设置-机构用户-用户管理', '1', '2015-05-02 20:56:22', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/index', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('eaaf30bd18a54d5499ec4cc1e3f7433b', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:31:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=360&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar11演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar11&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('ebed0782ef824327867611345d20734d', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:31:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('ecaddf9bdfd64571842da9232a565f57', '1', '我的面板-个人信息-修改密码', '1', '2015-04-21 21:46:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/modifyPwd', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('eddc7f6b99b04dba88c675fd6e4b7234', '1', '代码生成-代码生成-生成方案配置', '1', '2015-04-21 22:14:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/gen/genScheme', 'GET', 'tabPageId=jerichotabiframe_18', '');
INSERT INTO `sys_log` VALUES ('edfa07e9928f43edac5af8187cb8dec3', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 21:39:09', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('ee1d271e12da4ce4b5c79cac27c3ed53', '1', '系统设置-系统设置-菜单管理-修改', '1', '2015-05-03 21:29:49', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/save', 'POST', 'id=&icon=&sort=180&parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff&name=bar5演示&target=&permission=&remarks=&href=/echartsdemo/echartsdemo/bar5&parent.name=bar&isShow=1', '');
INSERT INTO `sys_log` VALUES ('ee3e1690c9e640f8ba265679e8c185ee', '1', '系统设置-系统设置-角色管理-查看', '1', '2015-05-03 21:32:55', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/form', 'GET', 'id=1', '');
INSERT INTO `sys_log` VALUES ('ee428c3c408e4db880b87edd12f829c8', '1', '系统登录', '1', '2015-04-18 19:01:42', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('ee875ddf1da74f1bb54afb6c4936ff38', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 13:59:33', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('eea55d423f2041e58c63b3111fcbad31', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:45:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('ef313c8f28304840b9d8b62b880d8b65', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:36:30', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'GET', 'tabPageId=jerichotabiframe_7', '');
INSERT INTO `sys_log` VALUES ('ef381ee02b0a4468b4494548f7dfd592', '1', '系统设置-机构用户-用户管理-查看', '1', '2015-04-21 22:13:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/list', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('f0241072c55447c98f8e99af5dfee698', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:42:06', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_3', '');
INSERT INTO `sys_log` VALUES ('f0df67d3563f44229d62128b9247b48b', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:33:44', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('f19e083bd39749e0a3501eb4644a86c9', '1', 'echarts-line-line10演示', '1', '2015-05-02 21:36:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line10', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('f257399a8aeb40d9970ddc93812857f2', '1', 'echarts-bar-bar2演示', '1', '2015-05-03 21:33:36', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar2', 'GET', 'tabPageId=jerichotabiframe_6', '');
INSERT INTO `sys_log` VALUES ('f280b90357ee4ac89f50b818c9a4d2ef', '1', 'echarts-bar-bar1演示', '1', '2015-05-03 21:46:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('f2c5a42cfcd84624bdde1c84caad0f42', '1', '系统登录', '1', '2015-04-19 23:30:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('f2e54e1977bc46adb982a31dde432d87', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:00:30', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=18be3c3752744cf4b8be4f686e6b2586', '');
INSERT INTO `sys_log` VALUES ('f3983a9e83194943b387289daa615062', '1', '系统设置-系统设置-角色管理', '1', '2015-05-02 13:06:24', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('f424307d8fbb4179b3d43382ef5f2075', '1', 'echarts-demo演示-axis演示', '1', '2015-05-02 15:07:21', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('f4606b57439c4222b7f007c0289c88d9', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 15:09:42', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=1948167a2d504222b470899fbd139ec9', '');
INSERT INTO `sys_log` VALUES ('f56187fea4ae4f83b0562b2b5d8e668b', '1', '系统登录', '1', '2015-04-21 21:46:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('f5786e7caff345ff93a9701c970642c9', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:46:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('f6325aa6989f4d79a8fd8b60dc28bee3', '1', '系统设置-系统设置-角色管理', '1', '2015-05-03 21:32:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/role/', 'GET', 'tabPageId=jerichotabiframe_4', '');
INSERT INTO `sys_log` VALUES ('f6549b1adbda496c9dcd228a182d91f1', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-03 21:31:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('f6657a08fd1349509c2bd78afe3d09b5', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 13:51:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=8ec74143ca2d4263a11df0f52307671a', '');
INSERT INTO `sys_log` VALUES ('f668459e3c1b4a5ab5ddea28c48b18e2', '1', 'echarts-bar-bar12演示', '1', '2015-05-03 21:48:39', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar12', 'GET', 'tabPageId=jerichotabiframe_13', '');
INSERT INTO `sys_log` VALUES ('f7364f18996b46e0bc0fdc7a2afe2b4b', '1', '我的面板-个人信息-个人信息', '1', '2015-05-02 14:18:17', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/user/info', 'GET', 'tabPageId=jerichotabiframe_0', '');
INSERT INTO `sys_log` VALUES ('f7597b3df18042bbb9f70a216d7b7a44', '1', '监控-爬虫监控-爬虫节点监控', '1', '2015-05-02 13:55:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/monitor/monitorCrawlerClient', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('f7759a6bb27242548f1fdf2729814ad6', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-02 21:03:00', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'id=52f7cf9ca7e04db997fc0c1789a82f22', '');
INSERT INTO `sys_log` VALUES ('f7b79810373c433b980915e241bb0e8d', '1', '系统设置-机构用户-机构管理-查看', '1', '2015-05-02 14:18:41', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/office/form', 'GET', 'parent.id=', '');
INSERT INTO `sys_log` VALUES ('f7ba8923fde74d7f8c67c4268e94e867', '1', 'echarts-line-line5演示', '1', '2015-05-02 21:18:53', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line5', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('f857e1b93d074bf4aa0337eaafe327d9', '1', 'echarts-bar-bar演示', '1', '2015-05-02 21:34:34', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('f8822346c3644522969c945e3de3724d', '1', 'echarts-line-line演示', '1', '2015-05-02 21:15:05', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('f882a9ecaf1644dbbb443333c1913b78', '1', 'echarts-demo演示-line1演示', '1', '2015-05-02 20:58:08', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('f884fd9ba9764293929c060c8b27b441', '1', 'echarts-bar-bar5演示', '1', '2015-05-03 21:34:32', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar5', 'GET', 'tabPageId=jerichotabiframe_23', '');
INSERT INTO `sys_log` VALUES ('f95f81ce97f14873b0e79826c840129a', '1', 'echarts-line-line8演示', '1', '2015-05-02 21:41:32', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line8', 'GET', 'tabPageId=jerichotabiframe_9', '');
INSERT INTO `sys_log` VALUES ('f9947638210549289bb81681d72eac3a', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:17:51', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'GET', 'tabPageId=jerichotabiframe_19', '');
INSERT INTO `sys_log` VALUES ('f99d58e2ae0b49e09b821e40673549b6', '1', '系统设置-系统设置-菜单管理', '1', '2015-05-02 20:45:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/', 'GET', '', '');
INSERT INTO `sys_log` VALUES ('fa23be9a9fbc4ef0adc68e8692058452', '1', 'echarts-line-line4演示', '1', '2015-05-02 21:36:19', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line4', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('fad7b43f1afc4d8498b3aa6656d0e4df', '1', 'echarts-bar-bar1演示', '1', '2015-05-03 21:35:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar1', 'GET', 'tabPageId=jerichotabiframe_2', '');
INSERT INTO `sys_log` VALUES ('fafa81e83d36445eabc05af6a12ffb58', '1', 'echarts-bar-bar11演示', '1', '2015-05-03 21:36:01', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar11', 'GET', 'tabPageId=jerichotabiframe_16', '');
INSERT INTO `sys_log` VALUES ('fbf0c50064024b789bd251254960ef34', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:41:46', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'GET', 'tabPageId=jerichotabiframe_10', '');
INSERT INTO `sys_log` VALUES ('fccf6df1634d479382669b743acc40a1', '1', 'echarts-bar-bar演示', '1', '2015-05-03 21:33:27', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('fda14542190749abb1186241a340f974', '1', '系统设置-机构用户-区域管理', '1', '2015-04-21 22:13:52', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/area/', 'GET', 'tabPageId=jerichotabiframe_11', '');
INSERT INTO `sys_log` VALUES ('fee984b7b8794e238786250a85b2c1ba', '1', '系统设置-系统设置-菜单管理-查看', '1', '2015-05-03 21:28:50', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/sys/menu/form', 'GET', 'parent.id=f9863d7c6d3b49ce9b262e1b8ef8d0ff', '');
INSERT INTO `sys_log` VALUES ('ff15f1849b0a48bf813e781dc2b948ae', '1', 'echarts-demo演示-bar演示', '1', '2015-05-02 20:57:18', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar', 'GET', 'tabPageId=jerichotabiframe_1', '');
INSERT INTO `sys_log` VALUES ('ff2cd8a0c72e48a5a49699219ad213e6', '1', 'echarts-line-line9演示', '1', '2015-05-02 21:36:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line9', 'POST', '', '');
INSERT INTO `sys_log` VALUES ('ff48d263319a437ebe2e58452cb1f82d', '1', '系统登录', '1', '2015-05-02 15:07:47', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0', '/webcrawler-web/a', 'GET', 'login=', '');
INSERT INTO `sys_log` VALUES ('ffc7729a6fc0479287c6e9866fd2a8c1', '1', 'echarts-bar-bar11演示', '1', '2015-05-03 21:42:16', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/bar11', 'GET', 'tabPageId=jerichotabiframe_12', '');
INSERT INTO `sys_log` VALUES ('ffdd4be6a1e04b98bc289f55ee8e76a1', '1', 'echarts-demo演示-line演示', '1', '2015-05-02 20:51:31', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36', '/webcrawler-web/a/echartsdemo/echartsdemo/line', 'GET', 'tabPageId=jerichotabiframe_2', '');

-- ----------------------------
-- Table structure for `sys_mdict`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mdict`;
CREATE TABLE `sys_mdict` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_mdict_parent_id` (`parent_id`),
  KEY `sys_mdict_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='多级字典表';

-- ----------------------------
-- Records of sys_mdict
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `href` varchar(2000) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT NULL COMMENT '目标',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `is_show` char(1) NOT NULL COMMENT '是否在菜单中显示',
  `permission` varchar(200) DEFAULT NULL COMMENT '权限标识',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_menu_parent_id` (`parent_id`),
  KEY `sys_menu_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('0057c22f0fa34505843f138b73ecbf1a', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line8演示', '200', '/echartsdemo/echartsdemo/line8', '', '', '1', '', '1', '2015-05-02 21:13:46', '1', '2015-05-02 21:13:46', '', '0');
INSERT INTO `sys_menu` VALUES ('05f920cf61f643789d2c8b7a3f0c0a26', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line9演示', '230', '/echartsdemo/echartsdemo/line9', '', '', '1', '', '1', '2015-05-02 21:14:03', '1', '2015-05-02 21:14:03', '', '0');
INSERT INTO `sys_menu` VALUES ('06fc54514cdf45d992c1f5403215d016', '1', '0,1,', 'echarts', '300', '', '', '', '1', '', '1', '2015-05-02 14:24:02', '1', '2015-05-02 14:24:02', '', '0');
INSERT INTO `sys_menu` VALUES ('0b2ebd4d639e4c2b83c2dd0764522f24', 'ba8092291b40482db8fe7fc006ea3d76', '0,1,79,3c92c17886944d0687e73e286cada573,ba8092291b40482db8fe7fc006ea3d76,', '编辑', '60', '', '', '', '0', 'test:testData:edit', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('0c977789606641cab40750d0c7dacf25', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar9演示', '300', '/echartsdemo/echartsdemo/bar9', '', '', '1', '', '1', '2015-05-03 21:30:53', '1', '2015-05-03 21:30:53', '', '0');
INSERT INTO `sys_menu` VALUES ('0ca004d6b1bf4bcab9670a5060d82a55', '3c92c17886944d0687e73e286cada573', '0,1,79,3c92c17886944d0687e73e286cada573,', '树结构', '90', '/test/testTree', '', '', '1', '', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('1', '0', '0,', '功能菜单', '0', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '3', '0,1,2,3,', '字典管理', '60', '/sys/dict/', null, 'th-list', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '10', '0,1,2,3,10,', '查看', '30', null, null, null, '0', 'sys:dict:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '10', '0,1,2,3,10,', '修改', '40', null, null, null, '0', 'sys:dict:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('1225696cf2124e148e3daca733f25150', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar15演示', '450', '/echartsdemo/echartsdemo/bar15', '', '', '1', '', '1', '2015-05-03 21:32:50', '1', '2015-05-03 21:32:50', '', '0');
INSERT INTO `sys_menu` VALUES ('13', '2', '0,1,2,', '机构用户', '970', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '13', '0,1,2,13,', '区域管理', '50', '/sys/area/', null, 'th', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '14', '0,1,2,13,14,', '查看', '30', null, null, null, '0', 'sys:area:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '14', '0,1,2,13,14,', '修改', '40', null, null, null, '0', 'sys:area:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '13', '0,1,2,13,', '机构管理', '40', '/sys/office/', null, 'th-large', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '17', '0,1,2,13,17,', '查看', '30', null, null, null, '0', 'sys:office:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('18be3c3752744cf4b8be4f686e6b2586', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line1演示', '50', '/echartsdemo/echartsdemo/line1', '', '', '1', '', '1', '2015-05-02 20:56:54', '1', '2015-05-02 21:00:42', '', '0');
INSERT INTO `sys_menu` VALUES ('19', '17', '0,1,2,13,17,', '修改', '40', null, null, null, '0', 'sys:office:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('1948167a2d504222b470899fbd139ec9', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar演示', '30', '/echartsdemo/echartsdemo/bar', '', '', '1', '', '1', '2015-05-02 14:34:28', '1', '2015-05-02 21:04:21', '', '0');
INSERT INTO `sys_menu` VALUES ('1e1ce8b9bb3d4077b18219589848a7a7', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar10演示', '330', '/echartsdemo/echartsdemo/bar10', '', '', '1', '', '1', '2015-05-03 21:31:11', '1', '2015-05-03 21:31:11', '', '0');
INSERT INTO `sys_menu` VALUES ('2', '1', '0,1,', '系统设置', '900', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '13', '0,1,2,13,', '用户管理', '30', '/sys/user/index', null, 'user', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '20', '0,1,2,13,20,', '查看', '30', null, null, null, '0', 'sys:user:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '20', '0,1,2,13,20,', '修改', '40', null, null, null, '0', 'sys:user:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '2', '0,1,2,', '关于帮助', '990', null, null, null, '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('24', '23', '0,1,2,23', '官方首页', '30', 'http://jeesite.com', '_blank', null, '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('24ebfed0acae410a86c81628d2659a67', '06fc54514cdf45d992c1f5403215d016', '0,1,06fc54514cdf45d992c1f5403215d016,', 'line', '30', '', '', '', '1', '', '1', '2015-05-02 14:25:01', '1', '2015-05-02 21:03:43', '', '0');
INSERT INTO `sys_menu` VALUES ('25', '23', '0,1,2,23', '项目支持', '50', 'http://jeesite.com/donation.html', '_blank', null, '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('26', '23', '0,1,2,23', '论坛交流', '80', 'http://bbs.jeesite.com', '_blank', null, '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('26ad4334b3aa4183b916e7df07e928e6', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar3演示', '120', '/echartsdemo/echartsdemo/bar3', '', '', '1', '', '1', '2015-05-03 21:29:16', '1', '2015-05-03 21:29:16', '', '0');
INSERT INTO `sys_menu` VALUES ('26d21cfbdba14edc91e191eaa273c969', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line10演示', '260', '/echartsdemo/echartsdemo/line10', '', '', '1', '', '1', '2015-05-02 21:14:19', '1', '2015-05-02 21:14:19', '', '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '0,1,', '我的面板', '100', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('28', '27', '0,1,27,', '个人信息', '30', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('29', '28', '0,1,27,28,', '个人信息', '30', '/sys/user/info', null, 'user', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('2d617306f3ec41449eb5d1b8e23ee7db', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar14演示', '420', '/echartsdemo/echartsdemo/bar14', '', '', '1', '', '1', '2015-05-03 21:32:36', '1', '2015-05-03 21:32:36', '', '0');
INSERT INTO `sys_menu` VALUES ('2ddc9f9343734613ae373c55992e6603', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar5演示', '180', '/echartsdemo/echartsdemo/bar5', '', '', '1', '', '1', '2015-05-03 21:29:49', '1', '2015-05-03 21:29:49', '', '0');
INSERT INTO `sys_menu` VALUES ('3', '2', '0,1,2,', '系统设置', '980', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('30', '28', '0,1,27,28,', '修改密码', '40', '/sys/user/modifyPwd', null, 'lock', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('31', '1', '0,1,', '内容管理', '500', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('32', '31', '0,1,31,', '栏目设置', '990', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('33', '32', '0,1,31,32', '栏目管理', '30', '/cms/category/', null, 'align-justify', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('33105c1b026343f39e8572671fbc7b2d', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line3演示', '70', '/echartsdemo/echartsdemo/line3', '', '', '1', '', '1', '2015-05-02 21:11:58', '1', '2015-05-02 21:11:58', '', '0');
INSERT INTO `sys_menu` VALUES ('34', '33', '0,1,31,32,33,', '查看', '30', null, null, null, '0', 'cms:category:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('35', '33', '0,1,31,32,33,', '修改', '40', null, null, null, '0', 'cms:category:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('36', '32', '0,1,31,32', '站点设置', '40', '/cms/site/', null, 'certificate', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('37', '36', '0,1,31,32,36,', '查看', '30', null, null, null, '0', 'cms:site:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('38', '36', '0,1,31,32,36,', '修改', '40', null, null, null, '0', 'cms:site:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('39', '32', '0,1,31,32', '切换站点', '50', '/cms/site/select', null, 'retweet', '1', 'cms:site:select', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('3c92c17886944d0687e73e286cada573', '79', '0,1,79,', '生成示例', '120', '', '', '', '1', '', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('4', '3', '0,1,2,3,', '菜单管理', '30', '/sys/menu/', null, 'list-alt', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('40', '31', '0,1,31,', '内容管理', '500', null, null, null, '1', 'cms:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('41', '40', '0,1,31,40,', '内容发布', '30', '/cms/', null, 'briefcase', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('42', '41', '0,1,31,40,41,', '文章模型', '40', '/cms/article/', null, 'file', '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('43', '42', '0,1,31,40,41,42,', '查看', '30', null, null, null, '0', 'cms:article:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('44', '42', '0,1,31,40,41,42,', '修改', '40', null, null, null, '0', 'cms:article:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('45', '42', '0,1,31,40,41,42,', '审核', '50', null, null, null, '0', 'cms:article:audit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('46', '41', '0,1,31,40,41,', '链接模型', '60', '/cms/link/', null, 'random', '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('47', '46', '0,1,31,40,41,46,', '查看', '30', null, null, null, '0', 'cms:link:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('48', '46', '0,1,31,40,41,46,', '修改', '40', null, null, null, '0', 'cms:link:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('4855cf3b25c244fb8500a380db189d97', 'b1f6d1b86ba24365bae7fd86c5082317', '0,1,79,3c92c17886944d0687e73e286cada573,b1f6d1b86ba24365bae7fd86c5082317,', '查看', '30', '', '', '', '0', 'test:testDataMain:view', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('49', '46', '0,1,31,40,41,46,', '审核', '50', null, null, null, '0', 'cms:link:audit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('4ff0e0ce2dd74212b44f0d4f6cb6eba4', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar13演示', '390', '/echartsdemo/echartsdemo/bar13', '', '', '1', '', '1', '2015-05-03 21:32:21', '1', '2015-05-03 21:32:21', '', '0');
INSERT INTO `sys_menu` VALUES ('5', '4', '0,1,2,3,4,', '查看', '30', null, null, null, '0', 'sys:menu:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('50', '40', '0,1,31,40,', '评论管理', '40', '/cms/comment/?status=2', null, 'comment', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('51', '50', '0,1,31,40,50,', '查看', '30', null, null, null, '0', 'cms:comment:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('52', '50', '0,1,31,40,50,', '审核', '40', null, null, null, '0', 'cms:comment:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('529e700a03a54e2db0cd441c8ba93b4d', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar4演示', '150', '/echartsdemo/echartsdemo/bar4', '', '', '1', '', '1', '2015-05-03 21:29:33', '1', '2015-05-03 21:29:33', '', '0');
INSERT INTO `sys_menu` VALUES ('52f7cf9ca7e04db997fc0c1789a82f22', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line演示', '40', '/echartsdemo/echartsdemo/line', '', '', '1', '', '1', '2015-05-02 20:45:55', '1', '2015-05-02 20:45:55', '', '0');
INSERT INTO `sys_menu` VALUES ('53', '40', '0,1,31,40,', '公共留言', '80', '/cms/guestbook/?status=2', null, 'glass', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('54', '53', '0,1,31,40,53,', '查看', '30', null, null, null, '0', 'cms:guestbook:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('55', '53', '0,1,31,40,53,', '审核', '40', null, null, null, '0', 'cms:guestbook:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('56', '71', '0,1,27,71,', '文件管理', '90', '/../static/ckfinder/ckfinder.html', null, 'folder-open', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('57', '56', '0,1,27,40,56,', '查看', '30', null, null, null, '0', 'cms:ckfinder:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('58', '56', '0,1,27,40,56,', '上传', '40', null, null, null, '0', 'cms:ckfinder:upload', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('59', '56', '0,1,27,40,56,', '修改', '50', null, null, null, '0', 'cms:ckfinder:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('5a72a8ad0b6348b29fc5c2bad391b7c8', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar1演示', '60', '/echartsdemo/echartsdemo/bar1', '', '', '1', '', '1', '2015-05-03 21:28:27', '1', '2015-05-03 21:28:27', '', '0');
INSERT INTO `sys_menu` VALUES ('6', '4', '0,1,2,3,4,', '修改', '40', null, null, null, '0', 'sys:menu:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('60', '31', '0,1,31,', '统计分析', '600', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('61', '60', '0,1,31,60,', '信息量统计', '30', '/cms/stats/article', null, 'tasks', '1', 'cms:stats:article', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('62', '1', '0,1,', '在线办公', '200', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('63', '62', '0,1,62,', '个人办公', '30', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('64', '63', '0,1,62,63,', '请假办理', '300', '/oa/leave', null, 'leaf', '0', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('65', '64', '0,1,62,63,64,', '查看', '30', null, null, null, '0', 'oa:leave:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('66', '64', '0,1,62,63,64,', '修改', '40', null, null, null, '0', 'oa:leave:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('66a8428823df40a6b15d1179a4f86758', '8ec74143ca2d4263a11df0f52307671a', '0,1,e3234be734134047bc8770571d5d2c0d,8ec74143ca2d4263a11df0f52307671a,', '爬虫节点监控', '30', '/monitor/monitorCrawlerClient', '', '', '1', '', '1', '2015-05-02 13:51:34', '1', '2015-05-02 13:51:53', '', '0');
INSERT INTO `sys_menu` VALUES ('67', '2', '0,1,2,', '日志查询', '985', null, null, null, '1', null, '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('68', '67', '0,1,2,67,', '日志查询', '30', '/sys/log', null, 'pencil', '1', 'sys:log:view', '1', '2013-06-03 08:00:00', '1', '2013-06-03 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('69', '62', '0,1,62,', '流程管理', '300', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('699a4801a31e4963990f31d586b03c5a', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line5演示', '110', '/echartsdemo/echartsdemo/line5', '', '', '1', '', '1', '2015-05-02 21:12:48', '1', '2015-05-02 21:12:48', '', '0');
INSERT INTO `sys_menu` VALUES ('6a527edab58d48e09a85ea3e0b47ef44', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar7演示', '240', '/echartsdemo/echartsdemo/bar7', '', '', '1', '', '1', '2015-05-03 21:30:23', '1', '2015-05-03 21:30:23', '', '0');
INSERT INTO `sys_menu` VALUES ('7', '3', '0,1,2,3,', '角色管理', '50', '/sys/role/', null, 'lock', '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('70', '69', '0,1,62,69,', '流程管理', '50', '/act/process', null, 'road', '1', 'act:process:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('71', '27', '0,1,27,', '文件管理', '90', null, null, null, '1', null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('72', '69', '0,1,62,69,', '模型管理', '100', '/act/model', null, 'road', '1', 'act:model:edit', '1', '2013-09-20 08:00:00', '1', '2013-09-20 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('73', '63', '0,1,62,63,', '我的任务', '50', '/act/task/todo/', null, 'tasks', '1', null, '1', '2013-09-24 08:00:00', '1', '2013-09-24 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('735c0343973d487d8a3731f2f9918cbd', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line', '80', '', '', '', '1', '', '1', '2015-05-02 21:02:53', '1', '2015-05-02 21:02:53', '', '1');
INSERT INTO `sys_menu` VALUES ('74', '63', '0,1,62,63,', '审批测试', '100', '/oa/testAudit', null, null, '1', 'oa:testAudit:view,oa:testAudit:edit', '1', '2013-09-24 08:00:00', '1', '2013-09-24 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('75', '1', '0,1,', '在线演示', '3000', null, null, null, '1', null, '1', '2013-10-08 08:00:00', '1', '2013-10-08 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('79', '1', '0,1,', '代码生成', '5000', null, null, null, '1', null, '1', '2013-10-16 08:00:00', '1', '2013-10-16 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '7', '0,1,2,3,7,', '查看', '30', null, null, null, '0', 'sys:role:view', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('80', '79', '0,1,79,', '代码生成', '50', null, null, null, '1', null, '1', '2013-10-16 08:00:00', '1', '2013-10-16 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('81', '80', '0,1,79,80,', '生成方案配置', '30', '/gen/genScheme', null, null, '1', 'gen:genScheme:view,gen:genScheme:edit', '1', '2013-10-16 08:00:00', '1', '2013-10-16 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('82', '80', '0,1,79,80,', '业务表配置', '20', '/gen/genTable', null, null, '1', 'gen:genTable:view,gen:genTable:edit,gen:genTableColumn:view,gen:genTableColumn:edit', '1', '2013-10-16 08:00:00', '1', '2013-10-16 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('83', '80', '0,1,79,80,', '代码模板管理', '90', '/gen/genTemplate', null, null, '1', 'gen:genTemplate:view,gen:genTemplate:edit', '1', '2013-10-16 08:00:00', '1', '2013-10-16 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('84', '67', '0,1,2,67,', '连接池监视', '40', '/../druid', null, null, '1', null, '1', '2013-10-18 08:00:00', '1', '2013-10-18 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('85', '76', '0,1,75,76,', '行政区域', '80', '/../static/map/map-city.html', null, null, '1', null, '1', '2013-10-22 08:00:00', '1', '2013-10-22 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('86', '75', '0,1,75,', '组件演示', '50', null, null, null, '1', null, '1', '2013-10-22 08:00:00', '1', '2013-10-22 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('87', '86', '0,1,75,86,', '组件演示', '30', '/test/test/form', null, null, '1', 'test:test:view,test:test:edit', '1', '2013-10-22 08:00:00', '1', '2013-10-22 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('88', '62', '0,1,62,', '通知通告', '20', '', '', '', '1', '', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('89', '88', '0,1,62,88,', '我的通告', '30', '/oa/oaNotify/self', '', '', '1', '', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('8ec74143ca2d4263a11df0f52307671a', 'e3234be734134047bc8770571d5d2c0d', '0,1,e3234be734134047bc8770571d5d2c0d,', '爬虫监控', '10', '', '', '', '1', '', '1', '2015-05-02 13:51:01', '1', '2015-05-02 13:51:01', '', '0');
INSERT INTO `sys_menu` VALUES ('8f26e9cbba204846bedf0cab85e7dbe1', '06fc54514cdf45d992c1f5403215d016', '0,1,06fc54514cdf45d992c1f5403215d016,', 'bar11演示', '60', '/echartsdemo/echartsdemo/bar11', '', '', '1', '', '1', '2015-05-03 21:31:23', '1', '2015-05-03 21:31:23', '', '1');
INSERT INTO `sys_menu` VALUES ('9', '7', '0,1,2,3,7,', '修改', '40', null, null, null, '0', 'sys:role:edit', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_menu` VALUES ('90', '88', '0,1,62,88,', '通告管理', '50', '/oa/oaNotify', '', '', '1', 'oa:oaNotify:view,oa:oaNotify:edit', '1', '2013-11-08 08:00:00', '1', '2013-11-08 08:00:00', null, '1');
INSERT INTO `sys_menu` VALUES ('96d9deb3f23b4912ada0a40dc14fde00', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line6演示', '140', '/echartsdemo/echartsdemo/line6', '', '', '1', '', '1', '2015-05-02 21:13:09', '1', '2015-05-02 21:13:09', '', '0');
INSERT INTO `sys_menu` VALUES ('9ba7ffc521df4399a5929d1da9fa00ff', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar8演示', '270', '/echartsdemo/echartsdemo/bar8', '', '', '1', '', '1', '2015-05-03 21:30:36', '1', '2015-05-03 21:30:36', '', '0');
INSERT INTO `sys_menu` VALUES ('a0c75fcb32474a63a2a9406a6f0271cd', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line4演示', '80', '/echartsdemo/echartsdemo/line4', '', '', '1', '', '1', '2015-05-02 21:12:23', '1', '2015-05-02 21:12:23', '', '0');
INSERT INTO `sys_menu` VALUES ('a18cf4ac4625476fb6a8adc543c61757', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line11演示', '290', '/echartsdemo/echartsdemo/line11', '', '', '1', '', '1', '2015-05-02 21:14:34', '1', '2015-05-02 21:14:34', '', '0');
INSERT INTO `sys_menu` VALUES ('abfa302bd0b6485bb7b29b18144f4069', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line7演示', '170', '/echartsdemo/echartsdemo/line7', '', '', '1', '', '1', '2015-05-02 21:13:27', '1', '2015-05-02 21:13:27', '', '0');
INSERT INTO `sys_menu` VALUES ('afab2db430e2457f9cf3a11feaa8b869', '0ca004d6b1bf4bcab9670a5060d82a55', '0,1,79,3c92c17886944d0687e73e286cada573,0ca004d6b1bf4bcab9670a5060d82a55,', '编辑', '60', '', '', '', '0', 'test:testTree:edit', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('b0ad810ebeab49438b4882a3a6ef4bad', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar2演示', '90', '/echartsdemo/echartsdemo/bar2', '', '', '1', '', '1', '2015-05-03 21:28:45', '1', '2015-05-03 21:28:45', '', '0');
INSERT INTO `sys_menu` VALUES ('b1f6d1b86ba24365bae7fd86c5082317', '3c92c17886944d0687e73e286cada573', '0,1,79,3c92c17886944d0687e73e286cada573,', '主子表', '60', '/test/testDataMain', '', '', '1', '', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('ba8092291b40482db8fe7fc006ea3d76', '3c92c17886944d0687e73e286cada573', '0,1,79,3c92c17886944d0687e73e286cada573,', '单表', '30', '/test/testData', '', '', '1', '', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('c2e4d9082a0b4386884a0b203afe2c5c', '0ca004d6b1bf4bcab9670a5060d82a55', '0,1,79,3c92c17886944d0687e73e286cada573,0ca004d6b1bf4bcab9670a5060d82a55,', '查看', '30', '', '', '', '0', 'test:testTree:view', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('ceff2f72d4b94fb7a2fa05c8a7330190', '24ebfed0acae410a86c81628d2659a67', '0,1,06fc54514cdf45d992c1f5403215d016,24ebfed0acae410a86c81628d2659a67,', 'line2演示', '60', '/echartsdemo/echartsdemo/line2', '', '', '1', '', '1', '2015-05-02 21:11:24', '1', '2015-05-02 21:11:24', '', '0');
INSERT INTO `sys_menu` VALUES ('d15ec45a4c5449c3bbd7a61d5f9dd1d2', 'b1f6d1b86ba24365bae7fd86c5082317', '0,1,79,3c92c17886944d0687e73e286cada573,b1f6d1b86ba24365bae7fd86c5082317,', '编辑', '60', '', '', '', '0', 'test:testDataMain:edit', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('d60891075d314cdf96242bcd7aa17779', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar11演示', '360', '/echartsdemo/echartsdemo/bar11', '', '', '1', '', '1', '2015-05-03 21:31:47', '1', '2015-05-03 21:31:47', '', '0');
INSERT INTO `sys_menu` VALUES ('df7ce823c5b24ff9bada43d992f373e2', 'ba8092291b40482db8fe7fc006ea3d76', '0,1,79,3c92c17886944d0687e73e286cada573,ba8092291b40482db8fe7fc006ea3d76,', '查看', '30', '', '', '', '0', 'test:testData:view', '1', '2013-08-12 13:10:05', '1', '2013-08-12 13:10:05', '', '0');
INSERT INTO `sys_menu` VALUES ('e3234be734134047bc8770571d5d2c0d', '1', '0,1,', '监控', '200', '', '', '', '1', '', '1', '2015-05-02 13:49:39', '1', '2015-05-02 13:49:39', '', '0');
INSERT INTO `sys_menu` VALUES ('e4ef2b0096684902818f21b86fa17ede', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar6演示', '210', '/echartsdemo/echartsdemo/bar6', '', '', '1', '', '1', '2015-05-03 21:30:04', '1', '2015-05-03 21:30:04', '', '0');
INSERT INTO `sys_menu` VALUES ('e806056ed22f49a180575160df8771f7', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff', '0,1,06fc54514cdf45d992c1f5403215d016,f9863d7c6d3b49ce9b262e1b8ef8d0ff,', 'bar12演示', '370', '/echartsdemo/echartsdemo/bar12', '', '', '1', '', '1', '2015-05-03 21:32:09', '1', '2015-05-03 21:32:09', '', '0');
INSERT INTO `sys_menu` VALUES ('f9863d7c6d3b49ce9b262e1b8ef8d0ff', '06fc54514cdf45d992c1f5403215d016', '0,1,06fc54514cdf45d992c1f5403215d016,', 'bar', '20', '', '', '', '1', '', '1', '2015-05-02 21:04:08', '1', '2015-05-02 21:04:08', '', '0');

-- ----------------------------
-- Table structure for `sys_office`
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `area_id` varchar(64) NOT NULL COMMENT '归属区域',
  `code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `type` char(1) NOT NULL COMMENT '机构类型',
  `grade` char(1) NOT NULL COMMENT '机构等级',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) DEFAULT NULL COMMENT '邮政编码',
  `master` varchar(100) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(200) DEFAULT NULL COMMENT '电话',
  `fax` varchar(200) DEFAULT NULL COMMENT '传真',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `USEABLE` varchar(64) DEFAULT NULL COMMENT '是否启用',
  `PRIMARY_PERSON` varchar(64) DEFAULT NULL COMMENT '主负责人',
  `DEPUTY_PERSON` varchar(64) DEFAULT NULL COMMENT '副负责人',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_office_parent_id` (`parent_id`),
  KEY `sys_office_del_flag` (`del_flag`),
  KEY `sys_office_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构表';

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES ('1', '0', '0,', '山东省总公司', '10', '2', '100000', '1', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('10', '7', '0,1,7,', '市场部', '30', '3', '200003', '2', '2', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('11', '7', '0,1,7,', '技术部', '40', '3', '200004', '2', '2', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('12', '7', '0,1,7,', '历城区分公司', '0', '4', '201000', '1', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('13', '12', '0,1,7,12,', '公司领导', '10', '4', '201001', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('14', '12', '0,1,7,12,', '综合部', '20', '4', '201002', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('15', '12', '0,1,7,12,', '市场部', '30', '4', '201003', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('16', '12', '0,1,7,12,', '技术部', '40', '4', '201004', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('17', '7', '0,1,7,', '历下区分公司', '40', '5', '201010', '1', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('18', '17', '0,1,7,17,', '公司领导', '10', '5', '201011', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('19', '17', '0,1,7,17,', '综合部', '20', '5', '201012', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('2', '1', '0,1,', '公司领导', '10', '2', '100001', '2', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('20', '17', '0,1,7,17,', '市场部', '30', '5', '201013', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('21', '17', '0,1,7,17,', '技术部', '40', '5', '201014', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('22', '7', '0,1,7,', '高新区分公司', '50', '6', '201010', '1', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('23', '22', '0,1,7,22,', '公司领导', '10', '6', '201011', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('24', '22', '0,1,7,22,', '综合部', '20', '6', '201012', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('25', '22', '0,1,7,22,', '市场部', '30', '6', '201013', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('26', '22', '0,1,7,22,', '技术部', '40', '6', '201014', '2', '3', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('3', '1', '0,1,', '综合部', '20', '2', '100002', '2', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('4', '1', '0,1,', '市场部', '30', '2', '100003', '2', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('5', '1', '0,1,', '技术部', '40', '2', '100004', '2', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('6', '1', '0,1,', '研发部', '50', '2', '100005', '2', '1', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('7', '1', '0,1,', '济南市分公司', '20', '3', '200000', '1', '2', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('8', '7', '0,1,7,', '公司领导', '10', '3', '200001', '2', '2', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_office` VALUES ('9', '7', '0,1,7,', '综合部', '20', '3', '200002', '2', '2', null, null, null, null, null, null, '1', null, null, '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `office_id` varchar(64) DEFAULT NULL COMMENT '归属机构',
  `name` varchar(100) NOT NULL COMMENT '角色名称',
  `enname` varchar(255) DEFAULT NULL COMMENT '英文名称',
  `role_type` varchar(255) DEFAULT NULL COMMENT '角色类型',
  `data_scope` char(1) DEFAULT NULL COMMENT '数据范围',
  `is_sys` varchar(64) DEFAULT NULL COMMENT '是否系统数据',
  `useable` varchar(64) DEFAULT NULL COMMENT '是否可用',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_role_del_flag` (`del_flag`),
  KEY `sys_role_enname` (`enname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '2', '系统管理员', 'dept', 'assignment', '1', '1', '1', '1', '2013-05-27 08:00:00', '1', '2015-05-03 21:33:09', '', '0');
INSERT INTO `sys_role` VALUES ('2', '1', '公司管理员', 'hr', 'assignment', '2', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_role` VALUES ('3', '1', '本公司管理员', 'a', 'assignment', '3', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_role` VALUES ('4', '1', '部门管理员', 'b', 'assignment', '4', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_role` VALUES ('5', '1', '本部门管理员', 'c', 'assignment', '5', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_role` VALUES ('6', '1', '普通用户', 'd', 'assignment', '8', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_role` VALUES ('7', '7', '济南市管理员', 'e', 'assignment', '9', null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  `menu_id` varchar(64) NOT NULL COMMENT '菜单编号',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '0057c22f0fa34505843f138b73ecbf1a');
INSERT INTO `sys_role_menu` VALUES ('1', '05f920cf61f643789d2c8b7a3f0c0a26');
INSERT INTO `sys_role_menu` VALUES ('1', '06fc54514cdf45d992c1f5403215d016');
INSERT INTO `sys_role_menu` VALUES ('1', '0c977789606641cab40750d0c7dacf25');
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '1225696cf2124e148e3daca733f25150');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('1', '15');
INSERT INTO `sys_role_menu` VALUES ('1', '16');
INSERT INTO `sys_role_menu` VALUES ('1', '17');
INSERT INTO `sys_role_menu` VALUES ('1', '18');
INSERT INTO `sys_role_menu` VALUES ('1', '18be3c3752744cf4b8be4f686e6b2586');
INSERT INTO `sys_role_menu` VALUES ('1', '19');
INSERT INTO `sys_role_menu` VALUES ('1', '1948167a2d504222b470899fbd139ec9');
INSERT INTO `sys_role_menu` VALUES ('1', '1e1ce8b9bb3d4077b18219589848a7a7');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '20');
INSERT INTO `sys_role_menu` VALUES ('1', '21');
INSERT INTO `sys_role_menu` VALUES ('1', '22');
INSERT INTO `sys_role_menu` VALUES ('1', '24ebfed0acae410a86c81628d2659a67');
INSERT INTO `sys_role_menu` VALUES ('1', '26ad4334b3aa4183b916e7df07e928e6');
INSERT INTO `sys_role_menu` VALUES ('1', '26d21cfbdba14edc91e191eaa273c969');
INSERT INTO `sys_role_menu` VALUES ('1', '27');
INSERT INTO `sys_role_menu` VALUES ('1', '28');
INSERT INTO `sys_role_menu` VALUES ('1', '29');
INSERT INTO `sys_role_menu` VALUES ('1', '2d617306f3ec41449eb5d1b8e23ee7db');
INSERT INTO `sys_role_menu` VALUES ('1', '2ddc9f9343734613ae373c55992e6603');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '30');
INSERT INTO `sys_role_menu` VALUES ('1', '33105c1b026343f39e8572671fbc7b2d');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '4ff0e0ce2dd74212b44f0d4f6cb6eba4');
INSERT INTO `sys_role_menu` VALUES ('1', '5');
INSERT INTO `sys_role_menu` VALUES ('1', '529e700a03a54e2db0cd441c8ba93b4d');
INSERT INTO `sys_role_menu` VALUES ('1', '52f7cf9ca7e04db997fc0c1789a82f22');
INSERT INTO `sys_role_menu` VALUES ('1', '56');
INSERT INTO `sys_role_menu` VALUES ('1', '57');
INSERT INTO `sys_role_menu` VALUES ('1', '58');
INSERT INTO `sys_role_menu` VALUES ('1', '59');
INSERT INTO `sys_role_menu` VALUES ('1', '5a72a8ad0b6348b29fc5c2bad391b7c8');
INSERT INTO `sys_role_menu` VALUES ('1', '6');
INSERT INTO `sys_role_menu` VALUES ('1', '66a8428823df40a6b15d1179a4f86758');
INSERT INTO `sys_role_menu` VALUES ('1', '67');
INSERT INTO `sys_role_menu` VALUES ('1', '68');
INSERT INTO `sys_role_menu` VALUES ('1', '699a4801a31e4963990f31d586b03c5a');
INSERT INTO `sys_role_menu` VALUES ('1', '6a527edab58d48e09a85ea3e0b47ef44');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '71');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '8ec74143ca2d4263a11df0f52307671a');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '96d9deb3f23b4912ada0a40dc14fde00');
INSERT INTO `sys_role_menu` VALUES ('1', '9ba7ffc521df4399a5929d1da9fa00ff');
INSERT INTO `sys_role_menu` VALUES ('1', 'a0c75fcb32474a63a2a9406a6f0271cd');
INSERT INTO `sys_role_menu` VALUES ('1', 'a18cf4ac4625476fb6a8adc543c61757');
INSERT INTO `sys_role_menu` VALUES ('1', 'abfa302bd0b6485bb7b29b18144f4069');
INSERT INTO `sys_role_menu` VALUES ('1', 'b0ad810ebeab49438b4882a3a6ef4bad');
INSERT INTO `sys_role_menu` VALUES ('1', 'ceff2f72d4b94fb7a2fa05c8a7330190');
INSERT INTO `sys_role_menu` VALUES ('1', 'd60891075d314cdf96242bcd7aa17779');
INSERT INTO `sys_role_menu` VALUES ('1', 'e3234be734134047bc8770571d5d2c0d');
INSERT INTO `sys_role_menu` VALUES ('1', 'e4ef2b0096684902818f21b86fa17ede');
INSERT INTO `sys_role_menu` VALUES ('1', 'e806056ed22f49a180575160df8771f7');
INSERT INTO `sys_role_menu` VALUES ('1', 'f9863d7c6d3b49ce9b262e1b8ef8d0ff');
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '10');
INSERT INTO `sys_role_menu` VALUES ('2', '11');
INSERT INTO `sys_role_menu` VALUES ('2', '12');
INSERT INTO `sys_role_menu` VALUES ('2', '13');
INSERT INTO `sys_role_menu` VALUES ('2', '14');
INSERT INTO `sys_role_menu` VALUES ('2', '15');
INSERT INTO `sys_role_menu` VALUES ('2', '16');
INSERT INTO `sys_role_menu` VALUES ('2', '17');
INSERT INTO `sys_role_menu` VALUES ('2', '18');
INSERT INTO `sys_role_menu` VALUES ('2', '19');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '20');
INSERT INTO `sys_role_menu` VALUES ('2', '21');
INSERT INTO `sys_role_menu` VALUES ('2', '22');
INSERT INTO `sys_role_menu` VALUES ('2', '23');
INSERT INTO `sys_role_menu` VALUES ('2', '24');
INSERT INTO `sys_role_menu` VALUES ('2', '25');
INSERT INTO `sys_role_menu` VALUES ('2', '26');
INSERT INTO `sys_role_menu` VALUES ('2', '27');
INSERT INTO `sys_role_menu` VALUES ('2', '28');
INSERT INTO `sys_role_menu` VALUES ('2', '29');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '30');
INSERT INTO `sys_role_menu` VALUES ('2', '31');
INSERT INTO `sys_role_menu` VALUES ('2', '32');
INSERT INTO `sys_role_menu` VALUES ('2', '33');
INSERT INTO `sys_role_menu` VALUES ('2', '34');
INSERT INTO `sys_role_menu` VALUES ('2', '35');
INSERT INTO `sys_role_menu` VALUES ('2', '36');
INSERT INTO `sys_role_menu` VALUES ('2', '37');
INSERT INTO `sys_role_menu` VALUES ('2', '38');
INSERT INTO `sys_role_menu` VALUES ('2', '39');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '40');
INSERT INTO `sys_role_menu` VALUES ('2', '41');
INSERT INTO `sys_role_menu` VALUES ('2', '42');
INSERT INTO `sys_role_menu` VALUES ('2', '43');
INSERT INTO `sys_role_menu` VALUES ('2', '44');
INSERT INTO `sys_role_menu` VALUES ('2', '45');
INSERT INTO `sys_role_menu` VALUES ('2', '46');
INSERT INTO `sys_role_menu` VALUES ('2', '47');
INSERT INTO `sys_role_menu` VALUES ('2', '48');
INSERT INTO `sys_role_menu` VALUES ('2', '49');
INSERT INTO `sys_role_menu` VALUES ('2', '5');
INSERT INTO `sys_role_menu` VALUES ('2', '50');
INSERT INTO `sys_role_menu` VALUES ('2', '51');
INSERT INTO `sys_role_menu` VALUES ('2', '52');
INSERT INTO `sys_role_menu` VALUES ('2', '53');
INSERT INTO `sys_role_menu` VALUES ('2', '54');
INSERT INTO `sys_role_menu` VALUES ('2', '55');
INSERT INTO `sys_role_menu` VALUES ('2', '56');
INSERT INTO `sys_role_menu` VALUES ('2', '57');
INSERT INTO `sys_role_menu` VALUES ('2', '58');
INSERT INTO `sys_role_menu` VALUES ('2', '59');
INSERT INTO `sys_role_menu` VALUES ('2', '6');
INSERT INTO `sys_role_menu` VALUES ('2', '60');
INSERT INTO `sys_role_menu` VALUES ('2', '61');
INSERT INTO `sys_role_menu` VALUES ('2', '62');
INSERT INTO `sys_role_menu` VALUES ('2', '63');
INSERT INTO `sys_role_menu` VALUES ('2', '64');
INSERT INTO `sys_role_menu` VALUES ('2', '65');
INSERT INTO `sys_role_menu` VALUES ('2', '66');
INSERT INTO `sys_role_menu` VALUES ('2', '67');
INSERT INTO `sys_role_menu` VALUES ('2', '68');
INSERT INTO `sys_role_menu` VALUES ('2', '69');
INSERT INTO `sys_role_menu` VALUES ('2', '7');
INSERT INTO `sys_role_menu` VALUES ('2', '70');
INSERT INTO `sys_role_menu` VALUES ('2', '71');
INSERT INTO `sys_role_menu` VALUES ('2', '72');
INSERT INTO `sys_role_menu` VALUES ('2', '8');
INSERT INTO `sys_role_menu` VALUES ('2', '9');
INSERT INTO `sys_role_menu` VALUES ('3', '1');
INSERT INTO `sys_role_menu` VALUES ('3', '10');
INSERT INTO `sys_role_menu` VALUES ('3', '11');
INSERT INTO `sys_role_menu` VALUES ('3', '12');
INSERT INTO `sys_role_menu` VALUES ('3', '13');
INSERT INTO `sys_role_menu` VALUES ('3', '14');
INSERT INTO `sys_role_menu` VALUES ('3', '15');
INSERT INTO `sys_role_menu` VALUES ('3', '16');
INSERT INTO `sys_role_menu` VALUES ('3', '17');
INSERT INTO `sys_role_menu` VALUES ('3', '18');
INSERT INTO `sys_role_menu` VALUES ('3', '19');
INSERT INTO `sys_role_menu` VALUES ('3', '2');
INSERT INTO `sys_role_menu` VALUES ('3', '20');
INSERT INTO `sys_role_menu` VALUES ('3', '21');
INSERT INTO `sys_role_menu` VALUES ('3', '22');
INSERT INTO `sys_role_menu` VALUES ('3', '23');
INSERT INTO `sys_role_menu` VALUES ('3', '24');
INSERT INTO `sys_role_menu` VALUES ('3', '25');
INSERT INTO `sys_role_menu` VALUES ('3', '26');
INSERT INTO `sys_role_menu` VALUES ('3', '27');
INSERT INTO `sys_role_menu` VALUES ('3', '28');
INSERT INTO `sys_role_menu` VALUES ('3', '29');
INSERT INTO `sys_role_menu` VALUES ('3', '3');
INSERT INTO `sys_role_menu` VALUES ('3', '30');
INSERT INTO `sys_role_menu` VALUES ('3', '31');
INSERT INTO `sys_role_menu` VALUES ('3', '32');
INSERT INTO `sys_role_menu` VALUES ('3', '33');
INSERT INTO `sys_role_menu` VALUES ('3', '34');
INSERT INTO `sys_role_menu` VALUES ('3', '35');
INSERT INTO `sys_role_menu` VALUES ('3', '36');
INSERT INTO `sys_role_menu` VALUES ('3', '37');
INSERT INTO `sys_role_menu` VALUES ('3', '38');
INSERT INTO `sys_role_menu` VALUES ('3', '39');
INSERT INTO `sys_role_menu` VALUES ('3', '4');
INSERT INTO `sys_role_menu` VALUES ('3', '40');
INSERT INTO `sys_role_menu` VALUES ('3', '41');
INSERT INTO `sys_role_menu` VALUES ('3', '42');
INSERT INTO `sys_role_menu` VALUES ('3', '43');
INSERT INTO `sys_role_menu` VALUES ('3', '44');
INSERT INTO `sys_role_menu` VALUES ('3', '45');
INSERT INTO `sys_role_menu` VALUES ('3', '46');
INSERT INTO `sys_role_menu` VALUES ('3', '47');
INSERT INTO `sys_role_menu` VALUES ('3', '48');
INSERT INTO `sys_role_menu` VALUES ('3', '49');
INSERT INTO `sys_role_menu` VALUES ('3', '5');
INSERT INTO `sys_role_menu` VALUES ('3', '50');
INSERT INTO `sys_role_menu` VALUES ('3', '51');
INSERT INTO `sys_role_menu` VALUES ('3', '52');
INSERT INTO `sys_role_menu` VALUES ('3', '53');
INSERT INTO `sys_role_menu` VALUES ('3', '54');
INSERT INTO `sys_role_menu` VALUES ('3', '55');
INSERT INTO `sys_role_menu` VALUES ('3', '56');
INSERT INTO `sys_role_menu` VALUES ('3', '57');
INSERT INTO `sys_role_menu` VALUES ('3', '58');
INSERT INTO `sys_role_menu` VALUES ('3', '59');
INSERT INTO `sys_role_menu` VALUES ('3', '6');
INSERT INTO `sys_role_menu` VALUES ('3', '60');
INSERT INTO `sys_role_menu` VALUES ('3', '61');
INSERT INTO `sys_role_menu` VALUES ('3', '62');
INSERT INTO `sys_role_menu` VALUES ('3', '63');
INSERT INTO `sys_role_menu` VALUES ('3', '64');
INSERT INTO `sys_role_menu` VALUES ('3', '65');
INSERT INTO `sys_role_menu` VALUES ('3', '66');
INSERT INTO `sys_role_menu` VALUES ('3', '67');
INSERT INTO `sys_role_menu` VALUES ('3', '68');
INSERT INTO `sys_role_menu` VALUES ('3', '69');
INSERT INTO `sys_role_menu` VALUES ('3', '7');
INSERT INTO `sys_role_menu` VALUES ('3', '70');
INSERT INTO `sys_role_menu` VALUES ('3', '71');
INSERT INTO `sys_role_menu` VALUES ('3', '72');
INSERT INTO `sys_role_menu` VALUES ('3', '8');
INSERT INTO `sys_role_menu` VALUES ('3', '9');
INSERT INTO `sys_role_menu` VALUES ('4', '1');
INSERT INTO `sys_role_menu` VALUES ('4', '10');
INSERT INTO `sys_role_menu` VALUES ('4', '11');
INSERT INTO `sys_role_menu` VALUES ('4', '12');
INSERT INTO `sys_role_menu` VALUES ('4', '13');
INSERT INTO `sys_role_menu` VALUES ('4', '14');
INSERT INTO `sys_role_menu` VALUES ('4', '15');
INSERT INTO `sys_role_menu` VALUES ('4', '16');
INSERT INTO `sys_role_menu` VALUES ('4', '17');
INSERT INTO `sys_role_menu` VALUES ('4', '18');
INSERT INTO `sys_role_menu` VALUES ('4', '19');
INSERT INTO `sys_role_menu` VALUES ('4', '2');
INSERT INTO `sys_role_menu` VALUES ('4', '20');
INSERT INTO `sys_role_menu` VALUES ('4', '21');
INSERT INTO `sys_role_menu` VALUES ('4', '22');
INSERT INTO `sys_role_menu` VALUES ('4', '23');
INSERT INTO `sys_role_menu` VALUES ('4', '24');
INSERT INTO `sys_role_menu` VALUES ('4', '25');
INSERT INTO `sys_role_menu` VALUES ('4', '26');
INSERT INTO `sys_role_menu` VALUES ('4', '27');
INSERT INTO `sys_role_menu` VALUES ('4', '28');
INSERT INTO `sys_role_menu` VALUES ('4', '29');
INSERT INTO `sys_role_menu` VALUES ('4', '3');
INSERT INTO `sys_role_menu` VALUES ('4', '30');
INSERT INTO `sys_role_menu` VALUES ('4', '31');
INSERT INTO `sys_role_menu` VALUES ('4', '32');
INSERT INTO `sys_role_menu` VALUES ('4', '33');
INSERT INTO `sys_role_menu` VALUES ('4', '34');
INSERT INTO `sys_role_menu` VALUES ('4', '35');
INSERT INTO `sys_role_menu` VALUES ('4', '36');
INSERT INTO `sys_role_menu` VALUES ('4', '37');
INSERT INTO `sys_role_menu` VALUES ('4', '38');
INSERT INTO `sys_role_menu` VALUES ('4', '39');
INSERT INTO `sys_role_menu` VALUES ('4', '4');
INSERT INTO `sys_role_menu` VALUES ('4', '40');
INSERT INTO `sys_role_menu` VALUES ('4', '41');
INSERT INTO `sys_role_menu` VALUES ('4', '42');
INSERT INTO `sys_role_menu` VALUES ('4', '43');
INSERT INTO `sys_role_menu` VALUES ('4', '44');
INSERT INTO `sys_role_menu` VALUES ('4', '45');
INSERT INTO `sys_role_menu` VALUES ('4', '46');
INSERT INTO `sys_role_menu` VALUES ('4', '47');
INSERT INTO `sys_role_menu` VALUES ('4', '48');
INSERT INTO `sys_role_menu` VALUES ('4', '49');
INSERT INTO `sys_role_menu` VALUES ('4', '5');
INSERT INTO `sys_role_menu` VALUES ('4', '50');
INSERT INTO `sys_role_menu` VALUES ('4', '51');
INSERT INTO `sys_role_menu` VALUES ('4', '52');
INSERT INTO `sys_role_menu` VALUES ('4', '53');
INSERT INTO `sys_role_menu` VALUES ('4', '54');
INSERT INTO `sys_role_menu` VALUES ('4', '55');
INSERT INTO `sys_role_menu` VALUES ('4', '56');
INSERT INTO `sys_role_menu` VALUES ('4', '57');
INSERT INTO `sys_role_menu` VALUES ('4', '58');
INSERT INTO `sys_role_menu` VALUES ('4', '59');
INSERT INTO `sys_role_menu` VALUES ('4', '6');
INSERT INTO `sys_role_menu` VALUES ('4', '60');
INSERT INTO `sys_role_menu` VALUES ('4', '61');
INSERT INTO `sys_role_menu` VALUES ('4', '62');
INSERT INTO `sys_role_menu` VALUES ('4', '63');
INSERT INTO `sys_role_menu` VALUES ('4', '64');
INSERT INTO `sys_role_menu` VALUES ('4', '65');
INSERT INTO `sys_role_menu` VALUES ('4', '66');
INSERT INTO `sys_role_menu` VALUES ('4', '67');
INSERT INTO `sys_role_menu` VALUES ('4', '68');
INSERT INTO `sys_role_menu` VALUES ('4', '69');
INSERT INTO `sys_role_menu` VALUES ('4', '7');
INSERT INTO `sys_role_menu` VALUES ('4', '70');
INSERT INTO `sys_role_menu` VALUES ('4', '71');
INSERT INTO `sys_role_menu` VALUES ('4', '72');
INSERT INTO `sys_role_menu` VALUES ('4', '8');
INSERT INTO `sys_role_menu` VALUES ('4', '9');
INSERT INTO `sys_role_menu` VALUES ('5', '1');
INSERT INTO `sys_role_menu` VALUES ('5', '10');
INSERT INTO `sys_role_menu` VALUES ('5', '11');
INSERT INTO `sys_role_menu` VALUES ('5', '12');
INSERT INTO `sys_role_menu` VALUES ('5', '13');
INSERT INTO `sys_role_menu` VALUES ('5', '14');
INSERT INTO `sys_role_menu` VALUES ('5', '15');
INSERT INTO `sys_role_menu` VALUES ('5', '16');
INSERT INTO `sys_role_menu` VALUES ('5', '17');
INSERT INTO `sys_role_menu` VALUES ('5', '18');
INSERT INTO `sys_role_menu` VALUES ('5', '19');
INSERT INTO `sys_role_menu` VALUES ('5', '2');
INSERT INTO `sys_role_menu` VALUES ('5', '20');
INSERT INTO `sys_role_menu` VALUES ('5', '21');
INSERT INTO `sys_role_menu` VALUES ('5', '22');
INSERT INTO `sys_role_menu` VALUES ('5', '23');
INSERT INTO `sys_role_menu` VALUES ('5', '24');
INSERT INTO `sys_role_menu` VALUES ('5', '25');
INSERT INTO `sys_role_menu` VALUES ('5', '26');
INSERT INTO `sys_role_menu` VALUES ('5', '27');
INSERT INTO `sys_role_menu` VALUES ('5', '28');
INSERT INTO `sys_role_menu` VALUES ('5', '29');
INSERT INTO `sys_role_menu` VALUES ('5', '3');
INSERT INTO `sys_role_menu` VALUES ('5', '30');
INSERT INTO `sys_role_menu` VALUES ('5', '31');
INSERT INTO `sys_role_menu` VALUES ('5', '32');
INSERT INTO `sys_role_menu` VALUES ('5', '33');
INSERT INTO `sys_role_menu` VALUES ('5', '34');
INSERT INTO `sys_role_menu` VALUES ('5', '35');
INSERT INTO `sys_role_menu` VALUES ('5', '36');
INSERT INTO `sys_role_menu` VALUES ('5', '37');
INSERT INTO `sys_role_menu` VALUES ('5', '38');
INSERT INTO `sys_role_menu` VALUES ('5', '39');
INSERT INTO `sys_role_menu` VALUES ('5', '4');
INSERT INTO `sys_role_menu` VALUES ('5', '40');
INSERT INTO `sys_role_menu` VALUES ('5', '41');
INSERT INTO `sys_role_menu` VALUES ('5', '42');
INSERT INTO `sys_role_menu` VALUES ('5', '43');
INSERT INTO `sys_role_menu` VALUES ('5', '44');
INSERT INTO `sys_role_menu` VALUES ('5', '45');
INSERT INTO `sys_role_menu` VALUES ('5', '46');
INSERT INTO `sys_role_menu` VALUES ('5', '47');
INSERT INTO `sys_role_menu` VALUES ('5', '48');
INSERT INTO `sys_role_menu` VALUES ('5', '49');
INSERT INTO `sys_role_menu` VALUES ('5', '5');
INSERT INTO `sys_role_menu` VALUES ('5', '50');
INSERT INTO `sys_role_menu` VALUES ('5', '51');
INSERT INTO `sys_role_menu` VALUES ('5', '52');
INSERT INTO `sys_role_menu` VALUES ('5', '53');
INSERT INTO `sys_role_menu` VALUES ('5', '54');
INSERT INTO `sys_role_menu` VALUES ('5', '55');
INSERT INTO `sys_role_menu` VALUES ('5', '56');
INSERT INTO `sys_role_menu` VALUES ('5', '57');
INSERT INTO `sys_role_menu` VALUES ('5', '58');
INSERT INTO `sys_role_menu` VALUES ('5', '59');
INSERT INTO `sys_role_menu` VALUES ('5', '6');
INSERT INTO `sys_role_menu` VALUES ('5', '60');
INSERT INTO `sys_role_menu` VALUES ('5', '61');
INSERT INTO `sys_role_menu` VALUES ('5', '62');
INSERT INTO `sys_role_menu` VALUES ('5', '63');
INSERT INTO `sys_role_menu` VALUES ('5', '64');
INSERT INTO `sys_role_menu` VALUES ('5', '65');
INSERT INTO `sys_role_menu` VALUES ('5', '66');
INSERT INTO `sys_role_menu` VALUES ('5', '67');
INSERT INTO `sys_role_menu` VALUES ('5', '68');
INSERT INTO `sys_role_menu` VALUES ('5', '69');
INSERT INTO `sys_role_menu` VALUES ('5', '7');
INSERT INTO `sys_role_menu` VALUES ('5', '70');
INSERT INTO `sys_role_menu` VALUES ('5', '71');
INSERT INTO `sys_role_menu` VALUES ('5', '72');
INSERT INTO `sys_role_menu` VALUES ('5', '8');
INSERT INTO `sys_role_menu` VALUES ('5', '9');
INSERT INTO `sys_role_menu` VALUES ('6', '1');
INSERT INTO `sys_role_menu` VALUES ('6', '10');
INSERT INTO `sys_role_menu` VALUES ('6', '11');
INSERT INTO `sys_role_menu` VALUES ('6', '12');
INSERT INTO `sys_role_menu` VALUES ('6', '13');
INSERT INTO `sys_role_menu` VALUES ('6', '14');
INSERT INTO `sys_role_menu` VALUES ('6', '15');
INSERT INTO `sys_role_menu` VALUES ('6', '16');
INSERT INTO `sys_role_menu` VALUES ('6', '17');
INSERT INTO `sys_role_menu` VALUES ('6', '18');
INSERT INTO `sys_role_menu` VALUES ('6', '19');
INSERT INTO `sys_role_menu` VALUES ('6', '2');
INSERT INTO `sys_role_menu` VALUES ('6', '20');
INSERT INTO `sys_role_menu` VALUES ('6', '21');
INSERT INTO `sys_role_menu` VALUES ('6', '22');
INSERT INTO `sys_role_menu` VALUES ('6', '23');
INSERT INTO `sys_role_menu` VALUES ('6', '24');
INSERT INTO `sys_role_menu` VALUES ('6', '25');
INSERT INTO `sys_role_menu` VALUES ('6', '26');
INSERT INTO `sys_role_menu` VALUES ('6', '27');
INSERT INTO `sys_role_menu` VALUES ('6', '28');
INSERT INTO `sys_role_menu` VALUES ('6', '29');
INSERT INTO `sys_role_menu` VALUES ('6', '3');
INSERT INTO `sys_role_menu` VALUES ('6', '30');
INSERT INTO `sys_role_menu` VALUES ('6', '31');
INSERT INTO `sys_role_menu` VALUES ('6', '32');
INSERT INTO `sys_role_menu` VALUES ('6', '33');
INSERT INTO `sys_role_menu` VALUES ('6', '34');
INSERT INTO `sys_role_menu` VALUES ('6', '35');
INSERT INTO `sys_role_menu` VALUES ('6', '36');
INSERT INTO `sys_role_menu` VALUES ('6', '37');
INSERT INTO `sys_role_menu` VALUES ('6', '38');
INSERT INTO `sys_role_menu` VALUES ('6', '39');
INSERT INTO `sys_role_menu` VALUES ('6', '4');
INSERT INTO `sys_role_menu` VALUES ('6', '40');
INSERT INTO `sys_role_menu` VALUES ('6', '41');
INSERT INTO `sys_role_menu` VALUES ('6', '42');
INSERT INTO `sys_role_menu` VALUES ('6', '43');
INSERT INTO `sys_role_menu` VALUES ('6', '44');
INSERT INTO `sys_role_menu` VALUES ('6', '45');
INSERT INTO `sys_role_menu` VALUES ('6', '46');
INSERT INTO `sys_role_menu` VALUES ('6', '47');
INSERT INTO `sys_role_menu` VALUES ('6', '48');
INSERT INTO `sys_role_menu` VALUES ('6', '49');
INSERT INTO `sys_role_menu` VALUES ('6', '5');
INSERT INTO `sys_role_menu` VALUES ('6', '50');
INSERT INTO `sys_role_menu` VALUES ('6', '51');
INSERT INTO `sys_role_menu` VALUES ('6', '52');
INSERT INTO `sys_role_menu` VALUES ('6', '53');
INSERT INTO `sys_role_menu` VALUES ('6', '54');
INSERT INTO `sys_role_menu` VALUES ('6', '55');
INSERT INTO `sys_role_menu` VALUES ('6', '56');
INSERT INTO `sys_role_menu` VALUES ('6', '57');
INSERT INTO `sys_role_menu` VALUES ('6', '58');
INSERT INTO `sys_role_menu` VALUES ('6', '59');
INSERT INTO `sys_role_menu` VALUES ('6', '6');
INSERT INTO `sys_role_menu` VALUES ('6', '60');
INSERT INTO `sys_role_menu` VALUES ('6', '61');
INSERT INTO `sys_role_menu` VALUES ('6', '62');
INSERT INTO `sys_role_menu` VALUES ('6', '63');
INSERT INTO `sys_role_menu` VALUES ('6', '64');
INSERT INTO `sys_role_menu` VALUES ('6', '65');
INSERT INTO `sys_role_menu` VALUES ('6', '66');
INSERT INTO `sys_role_menu` VALUES ('6', '67');
INSERT INTO `sys_role_menu` VALUES ('6', '68');
INSERT INTO `sys_role_menu` VALUES ('6', '69');
INSERT INTO `sys_role_menu` VALUES ('6', '7');
INSERT INTO `sys_role_menu` VALUES ('6', '70');
INSERT INTO `sys_role_menu` VALUES ('6', '71');
INSERT INTO `sys_role_menu` VALUES ('6', '72');
INSERT INTO `sys_role_menu` VALUES ('6', '8');
INSERT INTO `sys_role_menu` VALUES ('6', '9');
INSERT INTO `sys_role_menu` VALUES ('7', '1');
INSERT INTO `sys_role_menu` VALUES ('7', '10');
INSERT INTO `sys_role_menu` VALUES ('7', '11');
INSERT INTO `sys_role_menu` VALUES ('7', '12');
INSERT INTO `sys_role_menu` VALUES ('7', '13');
INSERT INTO `sys_role_menu` VALUES ('7', '14');
INSERT INTO `sys_role_menu` VALUES ('7', '15');
INSERT INTO `sys_role_menu` VALUES ('7', '16');
INSERT INTO `sys_role_menu` VALUES ('7', '17');
INSERT INTO `sys_role_menu` VALUES ('7', '18');
INSERT INTO `sys_role_menu` VALUES ('7', '19');
INSERT INTO `sys_role_menu` VALUES ('7', '2');
INSERT INTO `sys_role_menu` VALUES ('7', '20');
INSERT INTO `sys_role_menu` VALUES ('7', '21');
INSERT INTO `sys_role_menu` VALUES ('7', '22');
INSERT INTO `sys_role_menu` VALUES ('7', '23');
INSERT INTO `sys_role_menu` VALUES ('7', '24');
INSERT INTO `sys_role_menu` VALUES ('7', '25');
INSERT INTO `sys_role_menu` VALUES ('7', '26');
INSERT INTO `sys_role_menu` VALUES ('7', '27');
INSERT INTO `sys_role_menu` VALUES ('7', '28');
INSERT INTO `sys_role_menu` VALUES ('7', '29');
INSERT INTO `sys_role_menu` VALUES ('7', '3');
INSERT INTO `sys_role_menu` VALUES ('7', '30');
INSERT INTO `sys_role_menu` VALUES ('7', '31');
INSERT INTO `sys_role_menu` VALUES ('7', '32');
INSERT INTO `sys_role_menu` VALUES ('7', '33');
INSERT INTO `sys_role_menu` VALUES ('7', '34');
INSERT INTO `sys_role_menu` VALUES ('7', '35');
INSERT INTO `sys_role_menu` VALUES ('7', '36');
INSERT INTO `sys_role_menu` VALUES ('7', '37');
INSERT INTO `sys_role_menu` VALUES ('7', '38');
INSERT INTO `sys_role_menu` VALUES ('7', '39');
INSERT INTO `sys_role_menu` VALUES ('7', '4');
INSERT INTO `sys_role_menu` VALUES ('7', '40');
INSERT INTO `sys_role_menu` VALUES ('7', '41');
INSERT INTO `sys_role_menu` VALUES ('7', '42');
INSERT INTO `sys_role_menu` VALUES ('7', '43');
INSERT INTO `sys_role_menu` VALUES ('7', '44');
INSERT INTO `sys_role_menu` VALUES ('7', '45');
INSERT INTO `sys_role_menu` VALUES ('7', '46');
INSERT INTO `sys_role_menu` VALUES ('7', '47');
INSERT INTO `sys_role_menu` VALUES ('7', '48');
INSERT INTO `sys_role_menu` VALUES ('7', '49');
INSERT INTO `sys_role_menu` VALUES ('7', '5');
INSERT INTO `sys_role_menu` VALUES ('7', '50');
INSERT INTO `sys_role_menu` VALUES ('7', '51');
INSERT INTO `sys_role_menu` VALUES ('7', '52');
INSERT INTO `sys_role_menu` VALUES ('7', '53');
INSERT INTO `sys_role_menu` VALUES ('7', '54');
INSERT INTO `sys_role_menu` VALUES ('7', '55');
INSERT INTO `sys_role_menu` VALUES ('7', '56');
INSERT INTO `sys_role_menu` VALUES ('7', '57');
INSERT INTO `sys_role_menu` VALUES ('7', '58');
INSERT INTO `sys_role_menu` VALUES ('7', '59');
INSERT INTO `sys_role_menu` VALUES ('7', '6');
INSERT INTO `sys_role_menu` VALUES ('7', '60');
INSERT INTO `sys_role_menu` VALUES ('7', '61');
INSERT INTO `sys_role_menu` VALUES ('7', '62');
INSERT INTO `sys_role_menu` VALUES ('7', '63');
INSERT INTO `sys_role_menu` VALUES ('7', '64');
INSERT INTO `sys_role_menu` VALUES ('7', '65');
INSERT INTO `sys_role_menu` VALUES ('7', '66');
INSERT INTO `sys_role_menu` VALUES ('7', '67');
INSERT INTO `sys_role_menu` VALUES ('7', '68');
INSERT INTO `sys_role_menu` VALUES ('7', '69');
INSERT INTO `sys_role_menu` VALUES ('7', '7');
INSERT INTO `sys_role_menu` VALUES ('7', '70');
INSERT INTO `sys_role_menu` VALUES ('7', '71');
INSERT INTO `sys_role_menu` VALUES ('7', '72');
INSERT INTO `sys_role_menu` VALUES ('7', '8');
INSERT INTO `sys_role_menu` VALUES ('7', '9');

-- ----------------------------
-- Table structure for `sys_role_office`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_office`;
CREATE TABLE `sys_role_office` (
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  `office_id` varchar(64) NOT NULL COMMENT '机构编号',
  PRIMARY KEY (`role_id`,`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色-机构';

-- ----------------------------
-- Records of sys_role_office
-- ----------------------------
INSERT INTO `sys_role_office` VALUES ('7', '10');
INSERT INTO `sys_role_office` VALUES ('7', '11');
INSERT INTO `sys_role_office` VALUES ('7', '12');
INSERT INTO `sys_role_office` VALUES ('7', '13');
INSERT INTO `sys_role_office` VALUES ('7', '14');
INSERT INTO `sys_role_office` VALUES ('7', '15');
INSERT INTO `sys_role_office` VALUES ('7', '16');
INSERT INTO `sys_role_office` VALUES ('7', '17');
INSERT INTO `sys_role_office` VALUES ('7', '18');
INSERT INTO `sys_role_office` VALUES ('7', '19');
INSERT INTO `sys_role_office` VALUES ('7', '20');
INSERT INTO `sys_role_office` VALUES ('7', '21');
INSERT INTO `sys_role_office` VALUES ('7', '22');
INSERT INTO `sys_role_office` VALUES ('7', '23');
INSERT INTO `sys_role_office` VALUES ('7', '24');
INSERT INTO `sys_role_office` VALUES ('7', '25');
INSERT INTO `sys_role_office` VALUES ('7', '26');
INSERT INTO `sys_role_office` VALUES ('7', '7');
INSERT INTO `sys_role_office` VALUES ('7', '8');
INSERT INTO `sys_role_office` VALUES ('7', '9');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `company_id` varchar(64) NOT NULL COMMENT '归属公司',
  `office_id` varchar(64) NOT NULL COMMENT '归属部门',
  `login_name` varchar(100) NOT NULL COMMENT '登录名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `no` varchar(100) DEFAULT NULL COMMENT '工号',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `email` varchar(200) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(200) DEFAULT NULL COMMENT '电话',
  `mobile` varchar(200) DEFAULT NULL COMMENT '手机',
  `user_type` char(1) DEFAULT NULL COMMENT '用户类型',
  `photo` varchar(1000) DEFAULT NULL COMMENT '用户头像',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登陆时间',
  `login_flag` varchar(64) DEFAULT NULL COMMENT '是否可登录',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_user_office_id` (`office_id`),
  KEY `sys_user_login_name` (`login_name`),
  KEY `sys_user_company_id` (`company_id`),
  KEY `sys_user_update_date` (`update_date`),
  KEY `sys_user_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '1', '2', 'thinkgem', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0001', '系统管理员', 'thinkgem@163.com', '8675', '8675', null, null, '0:0:0:0:0:0:0:1', '2015-05-03 21:46:43', '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', '最高管理员', '0');
INSERT INTO `sys_user` VALUES ('10', '7', '11', 'jn_jsb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0010', '济南技术部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('11', '12', '13', 'lc_admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0011', '济南历城领导', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('12', '12', '18', 'lx_admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0012', '济南历下领导', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('13', '22', '23', 'gx_admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0013', '济南高新领导', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('2', '1', '2', 'sd_admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0002', '管理员', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('3', '1', '3', 'sd_zhb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0003', '综合部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('4', '1', '4', 'sd_scb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0004', '市场部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('5', '1', '5', 'sd_jsb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0005', '技术部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('6', '1', '6', 'sd_yfb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0006', '研发部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('7', '7', '8', 'jn_admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0007', '济南领导', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('8', '7', '9', 'jn_zhb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0008', '济南综合部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_user` VALUES ('9', '7', '10', 'jn_scb', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0009', '济南市场部', null, null, null, null, null, null, null, '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` varchar(64) NOT NULL COMMENT '用户编号',
  `role_id` varchar(64) NOT NULL COMMENT '角色编号',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户-角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('1', '2');
INSERT INTO `sys_user_role` VALUES ('10', '2');
INSERT INTO `sys_user_role` VALUES ('11', '3');
INSERT INTO `sys_user_role` VALUES ('12', '4');
INSERT INTO `sys_user_role` VALUES ('13', '5');
INSERT INTO `sys_user_role` VALUES ('14', '6');
INSERT INTO `sys_user_role` VALUES ('2', '1');
INSERT INTO `sys_user_role` VALUES ('3', '2');
INSERT INTO `sys_user_role` VALUES ('4', '3');
INSERT INTO `sys_user_role` VALUES ('5', '4');
INSERT INTO `sys_user_role` VALUES ('6', '5');
INSERT INTO `sys_user_role` VALUES ('7', '2');
INSERT INTO `sys_user_role` VALUES ('7', '7');
INSERT INTO `sys_user_role` VALUES ('8', '2');
INSERT INTO `sys_user_role` VALUES ('9', '1');

-- ----------------------------
-- Table structure for `test_data`
-- ----------------------------
DROP TABLE IF EXISTS `test_data`;
CREATE TABLE `test_data` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `user_id` varchar(64) DEFAULT NULL COMMENT '归属用户',
  `office_id` varchar(64) DEFAULT NULL COMMENT '归属部门',
  `area_id` varchar(64) DEFAULT NULL COMMENT '归属区域',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `in_date` date DEFAULT NULL COMMENT '加入日期',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `test_data_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务数据表';

-- ----------------------------
-- Records of test_data
-- ----------------------------

-- ----------------------------
-- Table structure for `test_data_child`
-- ----------------------------
DROP TABLE IF EXISTS `test_data_child`;
CREATE TABLE `test_data_child` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `test_data_main_id` varchar(64) DEFAULT NULL COMMENT '业务主表ID',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `test_data_child_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务数据子表';

-- ----------------------------
-- Records of test_data_child
-- ----------------------------

-- ----------------------------
-- Table structure for `test_data_main`
-- ----------------------------
DROP TABLE IF EXISTS `test_data_main`;
CREATE TABLE `test_data_main` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `user_id` varchar(64) DEFAULT NULL COMMENT '归属用户',
  `office_id` varchar(64) DEFAULT NULL COMMENT '归属部门',
  `area_id` varchar(64) DEFAULT NULL COMMENT '归属区域',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `in_date` date DEFAULT NULL COMMENT '加入日期',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `test_data_main_del_flag` (`del_flag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='业务数据表';

-- ----------------------------
-- Records of test_data_main
-- ----------------------------

-- ----------------------------
-- Table structure for `test_tree`
-- ----------------------------
DROP TABLE IF EXISTS `test_tree`;
CREATE TABLE `test_tree` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `test_tree_del_flag` (`del_flag`),
  KEY `test_data_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='树结构表';

-- ----------------------------
-- Records of test_tree
-- ----------------------------
